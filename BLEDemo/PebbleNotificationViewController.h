//
//  PebbleNotificationViewController.h
//  RVPhotoCube
//
//  Created by Steven Skindell on 2/24/14.
//  Copyright (c) 2014 skindellmagic. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PebbleNotificationViewController : UIViewController
{
    
    
    
}

@property (strong,nonatomic) IBOutlet UIButton *onebutton;
@property (strong,nonatomic) IBOutlet UIButton *twobutton;
@property (strong,nonatomic) IBOutlet UIButton *threebutton;
@property (strong,nonatomic) IBOutlet UIButton *fourbutton;
@property (strong,nonatomic) IBOutlet UIButton *fivebutton;
@property (strong,nonatomic) IBOutlet UIButton *sixbutton;
@property (strong,nonatomic) IBOutlet UIButton *savebutton;
@property (strong, nonatomic) IBOutlet UITextField *testfield1;
@property (strong, nonatomic) IBOutlet UITextField *testfield2;
@property (strong, nonatomic) IBOutlet UITextField *testfield3;
@property (strong, nonatomic) IBOutlet UITextField *testfield4;
@property (strong, nonatomic) IBOutlet UITextField *testfield5;
@property (strong, nonatomic) IBOutlet UITextField *testfield6;
@property (strong, nonatomic) IBOutlet UIImageView *iview;
@property (strong,nonatomic) IBOutlet UIButton *saveallbutton;

-(IBAction)savebuttonclick:(id)sender;
-(IBAction)saveallbuttonclick:(id)sender;


-(IBAction)onebuttonclick:(id)sender;
-(IBAction)twobuttonclick:(id)sender;
-(IBAction)threebuttonclick:(id)sender;
-(IBAction)fourbuttonclick:(id)sender;
-(IBAction)fivebuttonclick:(id)sender;
-(IBAction)sixbuttonclick:(id)sender;


@end
