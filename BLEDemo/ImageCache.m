// ImageCache.m

#import "ImageCache.h"
@implementation ImageCache

+ (instancetype)sharedImageCache {
    static ImageCache *_sharedImageCache = nil;
    static dispatch_once_t onceToken;

    dispatch_once(&onceToken, ^{
        _sharedImageCache = [[ImageCache alloc]init];
    });
    return _sharedImageCache;
}
@end
