//
//  ChatTableViewController.h
//  BLEDemo


#import <UIKit/UIKit.h>
#import "SerialPort.h"

@interface ChatTableViewController : UITableViewController <UITextFieldDelegate, SerialPortDelegate>

@property (strong, nonatomic) IBOutlet UITextField *messageTextField;

- (void) initWithPeripherals: (NSMutableArray*) discoveredPeripherals;

- (IBAction)sendMessage:(id)sender;

@end
