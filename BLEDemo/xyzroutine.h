if ((xval>124-20 && xval<124+20) && (yval>125-20 && yval<124+20) & (zval>67-20 && zval<67+20))
{
    imageside.text = @"6";
   if([imagesettouse isEqualToString:@"5"]) {
        dice.image = [UIImage imageWithData:[[NSUserDefaults standardUserDefaults]objectForKey:kPhoto6]];
        Imgvalues = 5;
        notificationtext.text = six;
        if ([soundonoff isEqualToString:@"1"]) {
            dirPaths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
            docsDir = [dirPaths objectAtIndex:0];
            NSString *soundFilePath = [docsDir stringByAppendingPathComponent:kSound6];
            NSURL *soundFileURL = [NSURL fileURLWithPath:soundFilePath];
            NSError *error;
            audioPlayer = [[AVAudioPlayer alloc] initWithContentsOfURL:soundFileURL error:&error];
            [audioPlayer stop];
            audioPlayer.numberOfLoops = 1;
            [audioPlayer play];
        }

    }
    if([imagesettouse isEqualToString:@"4"]) {
        dice.image = [UIImage imageNamed:s4p6];
        Imgvalues = 5;
        notificationtext.text = @"Puppy";
        if ([soundonoff isEqualToString:@"1"]) {
            NSURL *url = [NSURL fileURLWithPath:[NSString stringWithFormat:s4a6, [[NSBundle mainBundle] resourcePath]]];
            NSError *error;
            audioPlayer = [[AVAudioPlayer alloc] initWithContentsOfURL:url error:&error];
            [audioPlayer stop];
            audioPlayer.numberOfLoops = 1;
            [audioPlayer play];
        }

    }
    if([imagesettouse isEqualToString:@"3"]) {
        dice.image = [UIImage imageNamed:s3p6];
        Imgvalues = 5;
        notificationtext.text = @"Taj Mahal";
        if ([soundonoff isEqualToString:@"1"]){
            NSURL *url = [NSURL fileURLWithPath:[NSString stringWithFormat:s3a6, [[NSBundle mainBundle] resourcePath]]];
            NSError *error;
            audioPlayer = [[AVAudioPlayer alloc] initWithContentsOfURL:url error:&error];
            [audioPlayer stop];
            audioPlayer.numberOfLoops = 1;
            [audioPlayer play];
        }

    }
    if([imagesettouse isEqualToString:@"2"]) {
        dice.image = [UIImage imageNamed:s2p6];
        notificationtext.text = @"6-Infinity";
          Imgvalues = 5;
        if ([soundonoff isEqualToString:@"1"]){
            NSURL *url = [NSURL fileURLWithPath:[NSString stringWithFormat:s2a6, [[NSBundle mainBundle] resourcePath]]];
            NSError *error;
            audioPlayer = [[AVAudioPlayer alloc] initWithContentsOfURL:url error:&error];
            [audioPlayer stop];
            audioPlayer.numberOfLoops = 1;
            [audioPlayer play];
        }
  
    }
    if([imagesettouse isEqualToString:@"1"])
    {
        dice.image = [UIImage imageNamed:s1p6];
        Imgvalues = 5;
        notificationtext.text = @"6-Orange";
        if ([soundonoff isEqualToString:@"1"]){
            NSURL *url = [NSURL fileURLWithPath:[NSString stringWithFormat:s1a6, [[NSBundle mainBundle] resourcePath]]];
            NSError *error;
            audioPlayer = [[AVAudioPlayer alloc] initWithContentsOfURL:url error:&error];
            [audioPlayer stop];
            audioPlayer.numberOfLoops = 1;
            [audioPlayer play];
        }
       
   
    }
}
if ((xval>126-20 && xval<126+20) && (yval>66-20 && yval<66+20) & (zval>128-20 && zval<128+20)){
    imageside.text = @"5";
    if([imagesettouse isEqualToString:@"5"]) {
        notificationtext.text = five;
        dice.image = [UIImage imageWithData:[[NSUserDefaults standardUserDefaults]objectForKey:kPhoto5]];
        Imgvalues = 4;
        if ([soundonoff isEqualToString:@"1"]) {
            dirPaths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
            docsDir = [dirPaths objectAtIndex:0];
            NSString *soundFilePath = [docsDir stringByAppendingPathComponent:kSound5];
            NSURL *soundFileURL = [NSURL fileURLWithPath:soundFilePath];
            NSError *error;
            audioPlayer = [[AVAudioPlayer alloc] initWithContentsOfURL:soundFileURL error:&error];
            [audioPlayer stop];
            audioPlayer.numberOfLoops = 1;
            [audioPlayer play];
        }

    }
    if([imagesettouse isEqualToString:@"4"]) {
        dice.image = [UIImage imageNamed:s4p5];
         Imgvalues = 4;
        notificationtext.text = @"Kitten";
        if ([soundonoff isEqualToString:@"1"]){
            NSURL *url = [NSURL fileURLWithPath:[NSString stringWithFormat:s4a5, [[NSBundle mainBundle] resourcePath]]];
            NSError *error;
            audioPlayer = [[AVAudioPlayer alloc] initWithContentsOfURL:url error:&error];
            [audioPlayer stop];
            audioPlayer.numberOfLoops = 1;
            [audioPlayer play];
        }

    }
    if([imagesettouse isEqualToString:@"3"]) {
        dice.image = [UIImage imageNamed:s3p5];
         Imgvalues = 4;
        notificationtext.text = @"Great Wall of China";
        if ([soundonoff isEqualToString:@"1"]){
            NSURL *url = [NSURL fileURLWithPath:[NSString stringWithFormat:s3a5, [[NSBundle mainBundle] resourcePath]]];
            NSError *error;
            audioPlayer = [[AVAudioPlayer alloc] initWithContentsOfURL:url error:&error];
            [audioPlayer stop];
            audioPlayer.numberOfLoops = 1;
            [audioPlayer play];
        }

    }
    if([imagesettouse isEqualToString:@"2"]) {
        dice.image = [UIImage imageNamed:s2p5];
         Imgvalues = 4;
        notificationtext.text = @"5-Star";
        if ([soundonoff isEqualToString:@"1"]){
            NSURL *url = [NSURL fileURLWithPath:[NSString stringWithFormat:s2a5, [[NSBundle mainBundle] resourcePath]]];
            NSError *error;
            audioPlayer = [[AVAudioPlayer alloc] initWithContentsOfURL:url error:&error];
            [audioPlayer stop];
            audioPlayer.numberOfLoops = 1;
            [audioPlayer play];
        }

    }
    if([imagesettouse isEqualToString:@"1"])
    {
        dice.image = [UIImage imageNamed:s1p5];
         Imgvalues = 4;
        notificationtext.text = @"5-Red";
        if ([soundonoff isEqualToString:@"1"]){
            NSURL *url = [NSURL fileURLWithPath:[NSString stringWithFormat:s1a5, [[NSBundle mainBundle] resourcePath]]];
            NSError *error;
            audioPlayer = [[AVAudioPlayer alloc] initWithContentsOfURL:url error:&error];
            [audioPlayer stop];
            audioPlayer.numberOfLoops = 1;
            [audioPlayer play];
        }

    }
}
if ((xval>65-20 && xval<65+20) && (yval>130-20 && yval<130+20) & (zval>133-20 && zval<133+20)){
    imageside.text = @"3";
    if([imagesettouse isEqualToString:@"5"]) {
        notificationtext.text = three;
        dice.image = [UIImage imageWithData:[[NSUserDefaults standardUserDefaults]objectForKey:kPhoto3]];
         Imgvalues = 2;
        if ([soundonoff isEqualToString:@"1"]) {
            
            dirPaths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
            docsDir = [dirPaths objectAtIndex:0];
            NSString *soundFilePath = [docsDir stringByAppendingPathComponent:kSound3];
            NSURL *soundFileURL = [NSURL fileURLWithPath:soundFilePath];
            NSError *error;
            audioPlayer = [[AVAudioPlayer alloc] initWithContentsOfURL:soundFileURL error:&error];
            [audioPlayer stop];
            audioPlayer.numberOfLoops = 1;
            [audioPlayer play];
        }

    }
    if([imagesettouse isEqualToString:@"4"]) {
        dice.image = [UIImage imageNamed:s4p3];
         Imgvalues = 2;
        notificationtext.text = @"Lamb";
        if ([soundonoff isEqualToString:@"1"]){
            NSURL *url = [NSURL fileURLWithPath:[NSString stringWithFormat:s4a3, [[NSBundle mainBundle] resourcePath]]];
            NSError *error;
            audioPlayer = [[AVAudioPlayer alloc] initWithContentsOfURL:url error:&error];
            [audioPlayer stop];
            audioPlayer.numberOfLoops = 1;
            [audioPlayer play];
        }

    }
    if([imagesettouse isEqualToString:@"3"]) {
        dice.image = [UIImage imageNamed:s3p4];
         Imgvalues = 2;
        notificationtext.text = @"Roman Colosseum";
        if ([soundonoff isEqualToString:@"1"]){
            NSURL *url = [NSURL fileURLWithPath:[NSString stringWithFormat:s3a4, [[NSBundle mainBundle] resourcePath]]];
            NSError *error;
            audioPlayer = [[AVAudioPlayer alloc] initWithContentsOfURL:url error:&error];
            [audioPlayer stop];
            audioPlayer.numberOfLoops = 1;
            [audioPlayer play];
        }
 
    }
    if([imagesettouse isEqualToString:@"2"]) {
        dice.image = [UIImage imageNamed:s2p3];
         Imgvalues = 2;
        notificationtext.text = @"3 Wavy Lines";
        if ([soundonoff isEqualToString:@"1"]){
    NSURL *url = [NSURL fileURLWithPath:[NSString stringWithFormat:s2a3, [[NSBundle mainBundle] resourcePath]]];
            NSError *error;
            audioPlayer = [[AVAudioPlayer alloc] initWithContentsOfURL:url error:&error];
            [audioPlayer stop];
            audioPlayer.numberOfLoops = 1;
            [audioPlayer play];
        }
   
    }
    if([imagesettouse isEqualToString:@"1"])
    {
        dice.image = [UIImage imageNamed:s1p3];
         Imgvalues = 2;
        notificationtext.text = @"3-Green";
        if ([soundonoff isEqualToString:@"1"]){
            NSURL *url = [NSURL fileURLWithPath:[NSString stringWithFormat:s1a3, [[NSBundle mainBundle] resourcePath]]];
            NSError *error;
            audioPlayer = [[AVAudioPlayer alloc] initWithContentsOfURL:url error:&error];
            [audioPlayer stop];
            audioPlayer.numberOfLoops = 1;
            [audioPlayer play];
        }
  
    }
}
if ((xval>193-20 && xval<193+20) && (yval>129-20 && yval<129+20) & (zval>127-20 && zval<127+20)){
    imageside.text = @"4";
    notificationtext.text = four;
    if([imagesettouse isEqualToString:@"5"]) {
        dice.image = [UIImage imageWithData:[[NSUserDefaults standardUserDefaults]objectForKey:kPhoto4]];
         Imgvalues = 3;
        if ([soundonoff isEqualToString:@"1"]) {
            dirPaths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
            docsDir = [dirPaths objectAtIndex:0];
            NSString *soundFilePath = [docsDir stringByAppendingPathComponent:kSound4];
            NSURL *soundFileURL = [NSURL fileURLWithPath:soundFilePath];
            NSError *error;
            audioPlayer = [[AVAudioPlayer alloc] initWithContentsOfURL:soundFileURL error:&error];
            [audioPlayer stop];
            audioPlayer.numberOfLoops = 1;
            [audioPlayer play];
        }
   
    }
    if([imagesettouse isEqualToString:@"4"]) {
        dice.image = [UIImage imageNamed:s4p4];
         Imgvalues = 3;
        notificationtext.text = @"Monkey";
        if ([soundonoff isEqualToString:@"1"]){
            NSURL *url = [NSURL fileURLWithPath:[NSString stringWithFormat:s4a4, [[NSBundle mainBundle] resourcePath]]];
            NSError *error;
            audioPlayer = [[AVAudioPlayer alloc] initWithContentsOfURL:url error:&error];
            [audioPlayer stop];
            audioPlayer.numberOfLoops = 1;
            [audioPlayer play];
        }
  
    }
    if([imagesettouse isEqualToString:@"3"]) {
        dice.image = [UIImage imageNamed:s3p3];
         Imgvalues = 3;
        notificationtext.text = @"Leaning Tower of Pisa";
        if ([soundonoff isEqualToString:@"1"]){
            NSURL *url = [NSURL fileURLWithPath:[NSString stringWithFormat:s3a3, [[NSBundle mainBundle] resourcePath]]];
            NSError *error;
            audioPlayer = [[AVAudioPlayer alloc] initWithContentsOfURL:url error:&error];
            [audioPlayer stop];
            audioPlayer.numberOfLoops = 1;
            [audioPlayer play];
        }
 
    }
    if([imagesettouse isEqualToString:@"2"]) {
        dice.image = [UIImage imageNamed:s2p4];
         Imgvalues = 3;
        notificationtext.text = @"Square";
        if ([soundonoff isEqualToString:@"1"]){
            NSURL *url = [NSURL fileURLWithPath:[NSString stringWithFormat:s2a4, [[NSBundle mainBundle] resourcePath]]];
            NSError *error;
            audioPlayer = [[AVAudioPlayer alloc] initWithContentsOfURL:url error:&error];
            [audioPlayer stop];
            audioPlayer.numberOfLoops = 1;
            [audioPlayer play];
        }
    
    }
    if([imagesettouse isEqualToString:@"1"])
    {
        dice.image = [UIImage imageNamed:s1p4];
         Imgvalues = 3;
        notificationtext.text = @"4-Blue";
        if ([soundonoff isEqualToString:@"1"]){
            NSURL *url = [NSURL fileURLWithPath:[NSString stringWithFormat:s1a4, [[NSBundle mainBundle] resourcePath]]];
            NSError *error;
            audioPlayer = [[AVAudioPlayer alloc] initWithContentsOfURL:url error:&error];
            [audioPlayer stop];
            audioPlayer.numberOfLoops = 1;
            [audioPlayer play];
        }
   
    }
}
if ((xval>127-20 && xval<127+20) && (yval>190-20 && yval<190+20) & (zval>129-20 && zval<129+20)){
    imageside.text = @"2";
    if([imagesettouse isEqualToString:@"5"]) {
        notificationtext.text = two;
        dice.image = [UIImage imageWithData:[[NSUserDefaults standardUserDefaults]objectForKey:kPhoto2]];
         Imgvalues = 1;
        if ([soundonoff isEqualToString:@"1"]) {
            dirPaths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
            docsDir = [dirPaths objectAtIndex:0];
            NSString *soundFilePath = [docsDir stringByAppendingPathComponent:kSound2];
            NSURL *soundFileURL = [NSURL fileURLWithPath:soundFilePath];
            NSError *error;
            audioPlayer = [[AVAudioPlayer alloc] initWithContentsOfURL:soundFileURL error:&error];
            [audioPlayer stop];
            audioPlayer.numberOfLoops = 1;
            [audioPlayer play];
        }
     
    }
    if([imagesettouse isEqualToString:@"4"]) {
        dice.image = [UIImage imageNamed:s4p2];
         Imgvalues = 1;
        notificationtext.text = @"Panda";
        if ([soundonoff isEqualToString:@"1"]){
            NSURL *url = [NSURL fileURLWithPath:[NSString stringWithFormat:s4a2, [[NSBundle mainBundle] resourcePath]]];
            NSError *error;
            audioPlayer = [[AVAudioPlayer alloc] initWithContentsOfURL:url error:&error];
            [audioPlayer stop];
            audioPlayer.numberOfLoops = 1;
            [audioPlayer play];
        }
    
    }
    if([imagesettouse isEqualToString:@"3"]) {
        dice.image = [UIImage imageNamed:s3p2];
        Imgvalues = 1;
        notificationtext.text = @"Stonehenge";
        if ([soundonoff isEqualToString:@"1"]){
            NSURL *url = [NSURL fileURLWithPath:[NSString stringWithFormat:s3a2, [[NSBundle mainBundle] resourcePath]]];
            NSError *error;
            audioPlayer = [[AVAudioPlayer alloc] initWithContentsOfURL:url error:&error];
            [audioPlayer stop];
            audioPlayer.numberOfLoops = 1;
            [audioPlayer play];
        }
   
    }
    if([imagesettouse isEqualToString:@"2"]) {
        dice.image = [UIImage imageNamed:s2p2];
        Imgvalues = 1;
        notificationtext.text = @"Plus";
        if ([soundonoff isEqualToString:@"1"]){
            NSURL *url = [NSURL fileURLWithPath:[NSString stringWithFormat:s2a2, [[NSBundle mainBundle] resourcePath]]];
            NSError *error;
            audioPlayer = [[AVAudioPlayer alloc] initWithContentsOfURL:url error:&error];
            [audioPlayer stop];
            audioPlayer.numberOfLoops = 1;
            [audioPlayer play];
        }
     
    }
    if([imagesettouse isEqualToString:@"1"])
    {
        dice.image = [UIImage imageNamed:s1p2];
        Imgvalues = 1;
        notificationtext.text = @"2-Pink";
        if ([soundonoff isEqualToString:@"1"]){
            NSURL *url = [NSURL fileURLWithPath:[NSString stringWithFormat:s1a2, [[NSBundle mainBundle] resourcePath]]];
            NSError *error;
            audioPlayer = [[AVAudioPlayer alloc] initWithContentsOfURL:url error:&error];
            [audioPlayer stop];
            audioPlayer.numberOfLoops = 1;
            [audioPlayer play];
        }
      
    }
}
if ((xval>129-20 && xval<129+20) && (yval>124-20 && yval<124+20) & (zval>191-20 && zval<191+20)){
    imageside.text = @"1";
    if([imagesettouse isEqualToString:@"5"]) {
        notificationtext.text = one;
        dice.image = [UIImage imageWithData:[[NSUserDefaults standardUserDefaults]objectForKey:kPhoto1]];
        Imgvalues = 0;
        if ([soundonoff isEqualToString:@"1"]) {
            dirPaths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
            docsDir = [dirPaths objectAtIndex:0];
            NSString *soundFilePath = [docsDir stringByAppendingPathComponent:kSound1];
            NSURL *soundFileURL = [NSURL fileURLWithPath:soundFilePath];
            NSError *error;
            audioPlayer = [[AVAudioPlayer alloc] initWithContentsOfURL:soundFileURL error:&error];
            [audioPlayer stop];
            audioPlayer.numberOfLoops = 1;
            [audioPlayer play];                    }
     
    }
   if([imagesettouse isEqualToString:@"4"]) {
        dice.image = [UIImage imageNamed:s4p1];
             Imgvalues = 0;
        notificationtext.text = @"Pig";
        if ([soundonoff isEqualToString:@"1"]){
            NSURL *url = [NSURL fileURLWithPath:[NSString stringWithFormat:s4a1, [[NSBundle mainBundle] resourcePath]]];
            NSError *error;
            audioPlayer = [[AVAudioPlayer alloc] initWithContentsOfURL:url error:&error];
            [audioPlayer stop];
            audioPlayer.numberOfLoops = 1;
            [audioPlayer play];
        }
  
    }
    if([imagesettouse isEqualToString:@"3"]) {
        dice.image = [UIImage imageNamed:s3p1];
             Imgvalues = 0;
        notificationtext.text = @"Giza Pyramid";
        if ([soundonoff isEqualToString:@"1"]){
            NSURL *url = [NSURL fileURLWithPath:[NSString stringWithFormat:s3a1, [[NSBundle mainBundle] resourcePath]]];
            NSError *error;
            audioPlayer = [[AVAudioPlayer alloc] initWithContentsOfURL:url error:&error];
            [audioPlayer stop];
            audioPlayer.numberOfLoops = 1;
            [audioPlayer play];
        }
  
   }
    if([imagesettouse isEqualToString:@"2"]) {
  //**      dice.image = [UIImage imageNamed:@"circlewatch.png"];
        dice.image = [UIImage imageNamed:s2p1];
             Imgvalues = 0;
        notificationtext.text = @"1-Circle";
        if ([soundonoff isEqualToString:@"1"]){
            NSURL *url = [NSURL fileURLWithPath:[NSString stringWithFormat:s2a1, [[NSBundle mainBundle] resourcePath]]];
            NSError *error;
            audioPlayer = [[AVAudioPlayer alloc] initWithContentsOfURL:url error:&error];
            [audioPlayer stop];
            audioPlayer.numberOfLoops = 1;
            [audioPlayer play];
        }
    
    }
    if([imagesettouse isEqualToString:@"1"])
    {
        dice.image = [UIImage imageNamed:s1p1];
             Imgvalues = 0;
       notificationtext.text = @"1-Yellow";
        if ([soundonoff isEqualToString:@"1"]){
            NSURL *url = [NSURL fileURLWithPath:[NSString stringWithFormat:s1a1, [[NSBundle mainBundle] resourcePath]]];
            NSError *error;
            audioPlayer = [[AVAudioPlayer alloc] initWithContentsOfURL:url error:&error];
            [audioPlayer stop];
            audioPlayer.numberOfLoops = 1;
            [audioPlayer play];
        }
      
    }
}
if ([imageside.text isEqualToString:previmagesettouse])
{
    //[self showMessage:[NSString stringWithFormat:@"Image Set to: %@", @"1"]];
    
    //[self showMessage:[NSString stringWithFormat:@"Image Set to: %@", imageside.text]];
    
    // do nothing if imageset has not changed
}
else
{
    // only vibrate if imageset has changed
    previmagesettouse = imageside.text;
    if ([imageside.text isEqualToString:@"1"])
    {
        numberofvibes = 1;
    }
    if ([imageside.text isEqualToString:@"2"])
    {
        numberofvibes = 2;
    }
    if ([imageside.text isEqualToString:@"3"])
    {
        numberofvibes = 3;
    }
    if ([imageside.text isEqualToString:@"4"])
    {
        numberofvibes = 4;
    }
    if ([imageside.text isEqualToString:@"5"])
    {
        numberofvibes = 5;
    }
    if ([imageside.text isEqualToString:@"6"])
    {
        numberofvibes = 6;
    }
    if ([vibrationonoff isEqualToString:@"1"])
    {
        if(numberofgroups > 0 && numberofvibes > 0)
        {
            groupCount = 0;
            bigTimer = [NSTimer scheduledTimerWithTimeInterval:1 target:self selector:@selector(startGroup) userInfo:nil repeats:YES];
        }
    }
    if ([pushnotificationoff isEqualToString:@"1"])
    {
        UILocalNotification *localNotification = [[UILocalNotification alloc]init];
        localNotification.fireDate = [NSDate dateWithTimeIntervalSinceNow:1];
        localNotification.alertBody = notificationtext.text;
        localNotification.timeZone = [NSTimeZone defaultTimeZone];
        [[UIApplication sharedApplication] scheduleLocalNotification:localNotification];
    }
}