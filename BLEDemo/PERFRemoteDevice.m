//
//  PERFRemoteDevice.m
//  BLEDemo
//
//  Created by Tomas Henriksson on 1/27/12.
//  Copyright (c) 2012 connectBlue. All rights reserved.
//

#import "PERFRemoteDevice.h"

@implementation PERFRemoteDevice
{
    unsigned char cnt;
}

@synthesize serialPort;
@synthesize timer;
@synthesize txSwitch;
@synthesize txSwitchOn;
@synthesize rxBytes;
@synthesize rxBitrate;
@synthesize txBytes;
@synthesize txBitrate;
@synthesize rxSequence;
@synthesize rxErrors;
@synthesize rxWaitingFirst;
@synthesize data;

- (PERFRemoteDevice*) initWithPort: (SerialPort*) port andDataLen: (NSInteger)dataLen
{
    unsigned char *buf = nil;
    int i;
    
    serialPort = port;
    
    rxBytes = 0;
    txBytes = 0;
    
    txSwitch = nil;
    txSwitchOn = FALSE;
    
    buf = malloc(dataLen);
    
    for(i = 0; i < dataLen; i++)
    {
        buf[i] = i;
    }
    
    cnt = i;
    
    data = [NSData  dataWithBytes:buf length:dataLen];
    
    free(buf);
    
    return self;
}

- (void) formatData: (bool) reset
{
    int i;

    if(reset)
    {
        cnt = 0;
    }
    
    for(i = 0; i < data.length; i++)
    {
        ((unsigned char*)data.bytes)[i] = cnt + i;
    }
    cnt += i;
}

@end
