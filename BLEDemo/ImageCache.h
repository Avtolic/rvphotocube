// ImageCache.h

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface ImageCache : NSObject

@property (strong, nonatomic) UIImage * image;
@property (strong, nonatomic) NSString * imageID;

+ (instancetype)sharedImageCache;

@end
