//
//  DemoViewController.h
//  BLEDemo

#import <UIKit/UIKit.h>
#import <CoreBluetooth/CoreBluetooth.h>

#import <Foundation/Foundation.h>
#import <AVFoundation/AVFoundation.h>





@interface DemoViewController : UIViewController <CBPeripheralDelegate, UIImagePickerControllerDelegate, UINavigationControllerDelegate, UIActionSheetDelegate> {
    
    AVAudioPlayer *audioPlayer;
    int groupCount, numberofgroups, _vibrateCount, numberofvibes;
    NSTimer * vibrateTimer, * bigTimer;
    UIAlertView *altView;
    
}

// @property (nonatomic, retain) AVAudioPlayer *player1;



@property(nonatomic,assign)int _vibrateCount;
@property (nonatomic, retain)NSTimer * vibrateTimer;



@property (strong, nonatomic) IBOutlet UIButton *helpbutton;
@property (strong,nonatomic) IBOutlet UIButton *onebutton;



@property (strong, nonatomic) IBOutlet UISegmentedControl *peripheralSegmentedControl;
@property (strong, nonatomic) IBOutlet UISegmentedControl *imageset;

@property (strong, nonatomic) IBOutlet UIActivityIndicatorView *activityIndicator;

@property (strong, nonatomic) IBOutlet UILabel *accRangeLabel;
@property (strong, nonatomic) IBOutlet UILabel *imageside;

@property (strong, nonatomic) IBOutlet UIProgressView *accXProgressView;
@property (strong, nonatomic) IBOutlet UIProgressView *accYProgressView;
@property (strong, nonatomic) IBOutlet UIProgressView *accZProgressView;

@property (strong, nonatomic) IBOutlet UILabel *tempLabel;
@property (strong, nonatomic) IBOutlet UILabel *batteryLabel;
@property (strong, nonatomic) IBOutlet UILabel *rssiLabel;

@property (strong, nonatomic) IBOutlet UISwitch *greenLedSwitch;
@property (strong, nonatomic) IBOutlet UISwitch *redLedSwitch;

@property (strong, nonatomic) IBOutlet UILabel *accX;
@property (strong, nonatomic) IBOutlet UILabel *accY;
@property (strong, nonatomic) IBOutlet UILabel *accZ;
@property (strong, nonatomic) IBOutlet UILabel *imagesetlabel;
@property (strong, nonatomic) IBOutlet UIImageView *dice;
@property (strong,nonatomic) IBOutlet UITextField *iset;
@property (strong,nonatomic) IBOutlet UIButton *savebutton;
@property (strong,nonatomic) IBOutlet UIButton *custombutton;
@property (strong,nonatomic) IBOutlet UIButton *custombutton3;
@property (strong, nonatomic) IBOutlet UIButton *custombutton2;

@property (strong, nonatomic) IBOutlet UISegmentedControl *sliderImgSelect;
@property (weak, nonatomic) IBOutlet UITextField *groupField;
@property (weak, nonatomic) IBOutlet UITextField *vibrationField;
@property (weak, nonatomic) IBOutlet UIButton *vibrateButton;
@property (strong, nonatomic) IBOutlet UITextField *notificationtext;


- (IBAction)selectPeripheral:(id)sender;

- (IBAction)greenLedSwitchChanged:(id)sender;
- (IBAction)redLedSwitchChanged:(id)sender;
- (void)imagesetIndexChanged;


- (void) initWithPeripherals: (NSMutableArray*) discoveredPeripherals;

-(IBAction)onebuttonclick:(id)sender;

- (IBAction)vibrate:(id)sender;

@end
