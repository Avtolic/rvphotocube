//
//  CharactTableViewController.h
//  BLEDemo
//


#import <UIKit/UIKit.h>

#import <CoreBluetooth/CoreBluetooth.h>

@interface CharactTableViewController : UITableViewController <CBPeripheralDelegate>

@property (nonatomic, weak) CBPeripheral*     peripheral;
@property (nonatomic, weak) CBService*        service;
- (IBAction)refresh:(id)sender;

@end
