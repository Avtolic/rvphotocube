//
//  ChatCell.h
//  BLEDemo
//

#import <UIKit/UIKit.h>

@interface ChatCell : UITableViewCell

@property (nonatomic, strong) IBOutlet UILabel *labelFrom;
@property (nonatomic, strong) IBOutlet UILabel *labelTime;
@property (nonatomic, strong) IBOutlet UILabel *labelMessage;

@end
