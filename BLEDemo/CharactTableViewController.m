//
//  CharactTableViewController.m
//  BLEDemo


#import "CharactTableViewController.h"
#import "BLEDefinitions.h"
#import "EditValueTableViewController.h"

typedef enum
{
    CHARACT_S_NOT_LOADED,
    CHARACT_S_DISAPPEARED,
    
    CHARACT_S_APPEARED,

    CHARACT_S_APPEARED_NO_CONNECT_PERIPH
    
} CHARACT_State;


@implementation CharactTableViewController
{
    CHARACT_State state;
    
    EditValueTableViewController *editValueTableViewController;
}

@synthesize peripheral;
@synthesize service;

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];

    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
 
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
    
   state = CHARACT_S_DISAPPEARED;
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    editValueTableViewController = nil;
    
    if((peripheral != nil) && (peripheral.state == CBPeripheralStateConnected) && (service != nil))
    {        
        peripheral.delegate = self;
        
        state = CHARACT_S_APPEARED;
        
        [peripheral discoverCharacteristics:nil forService:service];
    }
    else
    {        
        state = CHARACT_S_APPEARED_NO_CONNECT_PERIPH;
        
        [self.tableView reloadData];
    }
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    
    if(peripheral != nil)
        peripheral.delegate = nil;
}

- (void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
    
    state = CHARACT_S_DISAPPEARED;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    NSInteger n = 0;
    
    if((state == CHARACT_S_APPEARED) &&
       (service != nil) && (service.characteristics.count > 0))
    {
        n = service.characteristics.count;
    }
    
    return n;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    NSInteger n = 0;
    
    if((state == CHARACT_S_APPEARED) &&
       (service != nil) && (service.characteristics.count > section))
    {
        n = 3;
    }
    
    return n;
}

- (NSString*) tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    NSString *str = nil;
    
    if(service != nil)
    {
        switch(section)
        {
            case 0:
                str = strFromServiceUUID(service.UUID);
                break;
                
            default:
                break;
        }
    }
    
    return str;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"CharactCell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];

    // Configure the cell...
    
    if(service.characteristics.count > indexPath.section)
    {
        CBCharacteristic* charact = [service.characteristics objectAtIndex:indexPath.section];
        
        switch(indexPath.row)
        {            
        case 0:
            cell.textLabel.text = strFromCharacteristicUUID(service.UUID, charact.UUID);
            break;
            
        case 1:
            cell.textLabel.text = strFromCharacteristicValue(service.UUID, charact.UUID, charact.value);
            break;
            
        case 2:
            cell.textLabel.text = strFromCharacteristicProperties(charact.properties);
            break;
            
        default:
            break;
        }
    }
    
    return cell;
}

/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationFade];
    }   
    else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath
{
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Navigation logic may go here. Create and push another view controller.
    /*
     <#DetailViewController#> *detailViewController = [[<#DetailViewController#> alloc] initWithNibName:@"<#Nib name#>" bundle:nil];
     // ...
     // Pass the selected object to the new view controller.
     [self.navigationController pushViewController:detailViewController animated:YES];
     */
    
    editValueTableViewController.peripheral = peripheral;
    editValueTableViewController.service = service;
    editValueTableViewController.characteristic =  [service.characteristics objectAtIndex:indexPath.section];
    
    editValueTableViewController.strServiceUuid = strFromServiceUUID(service.UUID);
    editValueTableViewController.strCharactUuid = strFromCharacteristicUUID(service.UUID, editValueTableViewController.characteristic.UUID);
    
    [tableView deselectRowAtIndexPath:indexPath animated:YES];

}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([segue.identifier isEqualToString:@"EditValueSegue"])
    {
        editValueTableViewController = segue.destinationViewController;
    }
}


- (void)peripheral:(CBPeripheral *)periph didDiscoverCharacteristicsForService:(CBService *)serv error:(NSError *)error
{
    CBCharacteristic* charact;
    
    for(int i = 0; i < service.characteristics.count; i++)
    {
        charact = [service.characteristics objectAtIndex:i];

        [peripheral readValueForCharacteristic: charact];
    }
}

- (void)peripheral:(CBPeripheral *)peripheral didUpdateValueForCharacteristic:(CBCharacteristic *)characteristic error:(NSError *)error
{
    [self.tableView reloadData];
}


- (IBAction)refresh:(id)sender
{
    if((state == CHARACT_S_APPEARED) &&
       (peripheral != nil) && (peripheral.state == CBPeripheralStateConnected) &&
       (service != nil))
    {               
        [peripheral discoverCharacteristics:nil forService:service];
    }
    else
    {        
        state = CHARACT_S_APPEARED_NO_CONNECT_PERIPH;
    }
}
@end
