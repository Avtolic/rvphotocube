//
//  SettingsViewController.m
//  RVPhotoCube
//
//  Created by Steven Skindell on 5/13/15.
//  Copyright (c) 2015 skindellmagic. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "SettingsViewController.h"

#define sw1 @"vibrationswitchonoff"
#define sw2 @"soundswitchonoff"
#define sw3 @"notificationswitchonoff"
#define sw4 @"watchswitchonoff"



NSArray *dirPaths;
NSString *docsDir;

@interface SettingsViewController ()

@end

@implementation SettingsViewController

{
    
}
@synthesize vibrationswitch;
@synthesize soundswitch;
@synthesize notificationswitch;
@synthesize watchswitch;
@synthesize saveallbutton;


- (void)viewDidLoad
{
    [super viewDidLoad];
    
    //hide titlebar
    // [[UIApplication sharedApplication] setStatusBarHidden:YES withAnimation:UIStatusBarAnimationNone];
    
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
   
    
    BOOL soundenabled = [defaults boolForKey:@"soundenabled"];
    BOOL vibrationenabled = [defaults boolForKey:@"vibrationenabled"];
    BOOL pushNotificationenabled = [defaults boolForKey:@"pushnotification"];
    BOOL picturedisplay = [defaults boolForKey:@"picturedisplay"];
    
    if (soundenabled){
        
        [self.soundswitch setOn:YES animated:NO];
    }
    
    if (vibrationenabled)
    {
       [self.vibrationswitch setOn:YES animated:NO];
    }
    
    if (pushNotificationenabled)
    {
       [self.notificationswitch setOn:YES animated:NO];
    }
    if (picturedisplay){
        [self.watchswitch setOn:YES animated:NO];
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(IBAction)vibrationswitchclick:(id)sender{
    
}


-(IBAction)soundswitchclick:(id)sender{
    
}
-(IBAction)notificationswitchclick:(id)sender{
    
}
-(IBAction)watchswitchclick:(id)sender{
    
}
-(IBAction)saveallbuttonclick:(id)sender
{
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    
    [defaults setObject:soundswitch forKey:@"soundenabled"];
    [defaults setObject:vibrationswitch forKey:@"vibrationenabled"];
    [defaults setObject:notificationswitch forKey:@"pushnotification"];
    [defaults setObject:watchswitch forKey:@"picturedisplay"];
 
    [defaults synchronize];

}



@end