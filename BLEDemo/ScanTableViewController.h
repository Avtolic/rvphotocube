//
//  ScanTableViewController.h
//  BLEDemo

#import <UIKit/UIKit.h>
#import <CoreBluetooth/CoreBluetooth.h>

@interface ScanTableViewController : UITableViewController <CBCentralManagerDelegate>
{

}
@property (strong,nonatomic) IBOutlet UIButton *exitprogram;

- (IBAction)clearPeripherals:(id)sender;

- (void) initWithPeripherals: (NSMutableArray*) discoveredPeripherals;

-(void) enterForeground;
-(void) enterBackground;

// Internal
- (void) clearPeriph;
- (void) clearPeriphForRow: (NSInteger)row;
- (void) scan: (bool)enable;
- (NSInteger)getRowForPeripheral: (CBPeripheral*)peripheral;
-(IBAction)closeprogramdown:(id) sender;

@end

