
#import "RecordingViewController.h"

#define kSound1 @"Sound1.caf"
#define kSound2 @"Sound2.caf"
#define kSound3 @"Sound3.caf"
#define kSound4 @"Sound4.caf"
#define kSound5 @"Sound5.caf"
#define kSound6 @"Sound6.caf"

#define kPhoto1 @"Photo1"
#define kPhoto2 @"Photo2"
#define kPhoto3 @"Photo3"
#define kPhoto4 @"Photo4"
#define kPhoto5 @"Photo5"
#define kPhoto6 @"Photo6"

static NSArray *dirPaths;
static NSString *docsDir;

@interface RecordingViewController ()

@end

@implementation RecordingViewController
{
    
}
@synthesize savebutton;
@synthesize stopbutton;
@synthesize recordbutton;
@synthesize playbutton;
@synthesize onebutton;
@synthesize twobutton;
@synthesize threebutton;
@synthesize fourbutton;
@synthesize fivebutton;
@synthesize sixbutton;
@synthesize testfield;
@synthesize iview;

NSString *testitout;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSString *idefault = [defaults objectForKey:@"lastimage"];
    if([idefault isEqualToString: @"1"]){
        testitout = @"1";
        [onebutton setBackgroundColor:[UIColor redColor]];
        
        dirPaths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
        docsDir = [dirPaths objectAtIndex:0];
        NSString *soundFilePath = [docsDir stringByAppendingPathComponent:kSound1];
        
        NSURL *soundFileURL = [NSURL fileURLWithPath:soundFilePath];
        
        NSDictionary *recordSettings = [NSDictionary
                                        dictionaryWithObjectsAndKeys:
                                        [NSNumber numberWithInt:AVAudioQualityMin],
                                        AVEncoderAudioQualityKey,
                                        [NSNumber numberWithInt:16],
                                        AVEncoderBitRateKey,
                                        [NSNumber numberWithInt: 2],
                                        AVNumberOfChannelsKey,
                                        [NSNumber numberWithFloat:44100.0],
                                        AVSampleRateKey,
                                        nil];
        
        NSError *error = nil;
        
        audioRecorder = [[AVAudioRecorder alloc]
                         initWithURL:soundFileURL
                         settings:recordSettings
                         error:&error];
        iview.image = [UIImage imageWithData:[[NSUserDefaults standardUserDefaults]objectForKey:kPhoto1]];
        
        
        testfield.text = testitout;

    }
    if([idefault isEqualToString: @"2"]){
        testitout = @"2";
        [twobutton setBackgroundColor:[UIColor redColor]];
        
        dirPaths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
        docsDir = [dirPaths objectAtIndex:0];
        NSString *soundFilePath = [docsDir stringByAppendingPathComponent:kSound2];
        
        NSURL *soundFileURL = [NSURL fileURLWithPath:soundFilePath];
        iview.image = [UIImage imageWithData:[[NSUserDefaults standardUserDefaults]objectForKey:kPhoto2]];

        
        NSDictionary *recordSettings = [NSDictionary
                                        dictionaryWithObjectsAndKeys:
                                        [NSNumber numberWithInt:AVAudioQualityMin],
                                        AVEncoderAudioQualityKey,
                                        [NSNumber numberWithInt:16],
                                        AVEncoderBitRateKey,
                                        [NSNumber numberWithInt: 2],
                                        AVNumberOfChannelsKey,
                                        [NSNumber numberWithFloat:44100.0],
                                        AVSampleRateKey,
                                        nil];
        
        NSError *error = nil;
        
        audioRecorder = [[AVAudioRecorder alloc]
                         initWithURL:soundFileURL
                         settings:recordSettings
                         error:&error];
        
        
        testfield.text = testitout;

    }

    if([idefault isEqualToString: @"3"]){
        testitout = @"3";
        [threebutton setBackgroundColor:[UIColor redColor]];
        
        dirPaths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
        docsDir = [dirPaths objectAtIndex:0];
        NSString *soundFilePath = [docsDir stringByAppendingPathComponent:kSound3];
        
        NSURL *soundFileURL = [NSURL fileURLWithPath:soundFilePath];
        
        NSDictionary *recordSettings = [NSDictionary
                                        dictionaryWithObjectsAndKeys:
                                        [NSNumber numberWithInt:AVAudioQualityMin],
                                        AVEncoderAudioQualityKey,
                                        [NSNumber numberWithInt:16],
                                        AVEncoderBitRateKey,
                                        [NSNumber numberWithInt: 2],
                                        AVNumberOfChannelsKey,
                                        [NSNumber numberWithFloat:44100.0],
                                        AVSampleRateKey,
                                        nil];
        
        NSError *error = nil;
        iview.image = [UIImage imageWithData:[[NSUserDefaults standardUserDefaults]objectForKey:kPhoto3]];

        
        audioRecorder = [[AVAudioRecorder alloc]
                         initWithURL:soundFileURL
                         settings:recordSettings
                         error:&error];
        
        
        testfield.text = testitout;

    }

    if([idefault isEqualToString: @"4"]){
        testitout = @"4";
        [fourbutton setBackgroundColor:[UIColor redColor]];
        
        dirPaths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
        docsDir = [dirPaths objectAtIndex:0];
        NSString *soundFilePath = [docsDir stringByAppendingPathComponent:kSound4];
        iview.image = [UIImage imageWithData:[[NSUserDefaults standardUserDefaults]objectForKey:kPhoto4]];

        
        NSURL *soundFileURL = [NSURL fileURLWithPath:soundFilePath];
        
        NSDictionary *recordSettings = [NSDictionary
                                        dictionaryWithObjectsAndKeys:
                                        [NSNumber numberWithInt:AVAudioQualityMin],
                                        AVEncoderAudioQualityKey,
                                        [NSNumber numberWithInt:16],
                                        AVEncoderBitRateKey,
                                        [NSNumber numberWithInt: 2],
                                        AVNumberOfChannelsKey,
                                        [NSNumber numberWithFloat:44100.0],
                                        AVSampleRateKey,
                                        nil];
        
        NSError *error = nil;
        
        audioRecorder = [[AVAudioRecorder alloc]
                         initWithURL:soundFileURL
                         settings:recordSettings
                         error:&error];
        
        
        testfield.text = testitout;

    }

    if([idefault isEqualToString: @"5"]){
        testitout = @"5";
        [fivebutton setBackgroundColor:[UIColor redColor]];
        
        dirPaths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
        docsDir = [dirPaths objectAtIndex:0];
        NSString *soundFilePath = [docsDir stringByAppendingPathComponent:kSound5];
        iview.image = [UIImage imageWithData:[[NSUserDefaults standardUserDefaults]objectForKey:kPhoto5]];

        
        NSURL *soundFileURL = [NSURL fileURLWithPath:soundFilePath];
        
        NSDictionary *recordSettings = [NSDictionary
                                        dictionaryWithObjectsAndKeys:
                                        [NSNumber numberWithInt:AVAudioQualityMin],
                                        AVEncoderAudioQualityKey,
                                        [NSNumber numberWithInt:16],
                                        AVEncoderBitRateKey,
                                        [NSNumber numberWithInt: 2],
                                        AVNumberOfChannelsKey,
                                        [NSNumber numberWithFloat:44100.0],
                                        AVSampleRateKey,
                                        nil];
        
        NSError *error = nil;
        
        audioRecorder = [[AVAudioRecorder alloc]
                         initWithURL:soundFileURL
                         settings:recordSettings
                         error:&error];
        
        
        testfield.text = testitout;

    }

    if([idefault isEqualToString: @"6"]){
        testitout = @"6";
        [sixbutton setBackgroundColor:[UIColor redColor]];
        
        dirPaths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
        docsDir = [dirPaths objectAtIndex:0];
        NSString *soundFilePath = [docsDir stringByAppendingPathComponent:kSound6];
        iview.image = [UIImage imageWithData:[[NSUserDefaults standardUserDefaults]objectForKey:kPhoto6]];

        NSURL *soundFileURL = [NSURL fileURLWithPath:soundFilePath];
        
        NSDictionary *recordSettings = [NSDictionary
                                        dictionaryWithObjectsAndKeys:
                                        [NSNumber numberWithInt:AVAudioQualityMin],
                                        AVEncoderAudioQualityKey,
                                        [NSNumber numberWithInt:16],
                                        AVEncoderBitRateKey,
                                        [NSNumber numberWithInt: 2],
                                        AVNumberOfChannelsKey,
                                        [NSNumber numberWithFloat:44100.0],
                                        AVSampleRateKey,
                                        nil];
        
        NSError *error = nil;
        
        audioRecorder = [[AVAudioRecorder alloc]
                         initWithURL:soundFileURL
                         settings:recordSettings
                         error:&error];
        
        
        testfield.text = testitout;

    }

    
    playbutton.enabled = YES;
    stopbutton.enabled = YES;
    
	// Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)recordbuttonclick:(id)sender {
    
    testitout = @"RECORD";
    [savebutton setBackgroundColor:[UIColor cyanColor]];
    [recordbutton setBackgroundColor:[UIColor redColor]];
    [playbutton setBackgroundColor:[UIColor cyanColor]];
    [stopbutton setBackgroundColor:[UIColor cyanColor]];
    
    
    AVAudioSession *audioSession = [AVAudioSession sharedInstance];
    [audioSession setCategory:AVAudioSessionCategoryPlayAndRecord error:nil];
    [audioSession setActive:YES error:nil];
    
    [audioRecorder prepareToRecord];
    if (!audioRecorder.recording)
    {
        playbutton.enabled = YES;
        stopbutton.enabled = YES;
        [audioRecorder record];
    }

        testfield.text = testitout;
    
}
- (IBAction)savebuttonclick:(id)sender {
    testitout = @"SAVE";
    [savebutton setBackgroundColor:[UIColor redColor]];
    [recordbutton setBackgroundColor:[UIColor cyanColor]];
    [playbutton setBackgroundColor:[UIColor cyanColor]];
    [stopbutton setBackgroundColor:[UIColor cyanColor]];
    
        testfield.text = testitout;
    
}
- (IBAction)playbuttonclick:(id)sender{
    
    testitout = @"PLAY";
    [savebutton setBackgroundColor:[UIColor cyanColor]];
    [recordbutton setBackgroundColor:[UIColor cyanColor]];
    [playbutton setBackgroundColor:[UIColor redColor]];
    [stopbutton setBackgroundColor:[UIColor cyanColor]];
    if (!audioRecorder.recording)
    {
        stopbutton.enabled = YES;
        recordbutton.enabled = YES;
        NSError *error;
               
        audioPlayer = [[AVAudioPlayer alloc]
                       initWithContentsOfURL:audioRecorder.url
                       error:&error];
        
        audioPlayer.delegate = self;
        
            [audioPlayer play];
    }

        testfield.text = testitout;
}
- (IBAction)stopbuttonclick:(id)sender{
    testitout = @"STOP";
    [savebutton setBackgroundColor:[UIColor cyanColor]];
    [recordbutton setBackgroundColor:[UIColor cyanColor]];
    [playbutton setBackgroundColor:[UIColor cyanColor]];
    [stopbutton setBackgroundColor:[UIColor redColor]];
    stopbutton.enabled = YES;
    playbutton.enabled = YES;
    recordbutton.enabled = YES;
    
    if (audioRecorder.recording)
    {
        [audioRecorder stop];
    } else if (audioPlayer.playing) {
        [audioPlayer stop];
    }

        testfield.text = testitout;
    
}

-(IBAction)onebuttonclick:(id)sender {
    testitout= @"1";
     [onebutton setBackgroundColor:[UIColor redColor]];
     [twobutton setBackgroundColor:[UIColor cyanColor]];
     [threebutton setBackgroundColor:[UIColor cyanColor]];
     [fourbutton setBackgroundColor:[UIColor cyanColor]];
     [fivebutton setBackgroundColor:[UIColor cyanColor]];
     [sixbutton setBackgroundColor:[UIColor cyanColor]];
    playbutton.enabled = YES;
    stopbutton.enabled = YES;
    
   iview.image = [UIImage imageWithData:[[NSUserDefaults standardUserDefaults]objectForKey:kPhoto1]];
    
    dirPaths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    docsDir = [dirPaths objectAtIndex:0];
    NSString *soundFilePath = [docsDir stringByAppendingPathComponent:kSound1];
    
    NSURL *soundFileURL = [NSURL fileURLWithPath:soundFilePath];
    
    NSDictionary *recordSettings = [NSDictionary
                                    dictionaryWithObjectsAndKeys:
                                    [NSNumber numberWithInt:AVAudioQualityMin],
                                    AVEncoderAudioQualityKey,
                                    [NSNumber numberWithInt:16],
                                    AVEncoderBitRateKey,
                                    [NSNumber numberWithInt: 2],
                                    AVNumberOfChannelsKey,
                                    [NSNumber numberWithFloat:44100.0],
                                    AVSampleRateKey,
                                    nil];
    
    NSError *error = nil;
    
    audioRecorder = [[AVAudioRecorder alloc]
                     initWithURL:soundFileURL
                     settings:recordSettings
                     error:&error];
    
      
testfield.text = testitout;
    
}
-(IBAction)twobuttonclick:(id)sender{
    testitout = @"2";
    [onebutton setBackgroundColor:[UIColor cyanColor]];
    [twobutton setBackgroundColor:[UIColor redColor]];
    [threebutton setBackgroundColor:[UIColor cyanColor]];
    [fourbutton setBackgroundColor:[UIColor cyanColor]];
    [fivebutton setBackgroundColor:[UIColor cyanColor]];
    [sixbutton setBackgroundColor:[UIColor cyanColor]];
    playbutton.enabled = YES;
    stopbutton.enabled = YES;
     iview.image = [UIImage imageWithData:[[NSUserDefaults standardUserDefaults]objectForKey:kPhoto2]];
        
    dirPaths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    docsDir = [dirPaths objectAtIndex:0];
    NSString *soundFilePath = [docsDir stringByAppendingPathComponent:kSound2];
    
    NSURL *soundFileURL = [NSURL fileURLWithPath:soundFilePath];
    
    NSDictionary *recordSettings = [NSDictionary
                                    dictionaryWithObjectsAndKeys:
                                    [NSNumber numberWithInt:AVAudioQualityMin],
                                    AVEncoderAudioQualityKey,
                                    [NSNumber numberWithInt:16],
                                    AVEncoderBitRateKey,
                                    [NSNumber numberWithInt: 2],
                                    AVNumberOfChannelsKey,
                                    [NSNumber numberWithFloat:44100.0],
                                    AVSampleRateKey,
                                    nil];
    
    NSError *error = nil;
    
    audioRecorder = [[AVAudioRecorder alloc]
                     initWithURL:soundFileURL
                     settings:recordSettings
                     error:&error];
       


        testfield.text = testitout;
      
}
-(IBAction)threebuttonclick:(id)sender{
    
    testitout= @"3";
    [onebutton setBackgroundColor:[UIColor cyanColor]];
    [twobutton setBackgroundColor:[UIColor cyanColor]];
    [threebutton setBackgroundColor:[UIColor redColor]];
    [fourbutton setBackgroundColor:[UIColor cyanColor]];
    [fivebutton setBackgroundColor:[UIColor cyanColor]];
    [sixbutton setBackgroundColor:[UIColor cyanColor]];
    playbutton.enabled = YES;
    stopbutton.enabled = YES;
    
   
    
    dirPaths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    docsDir = [dirPaths objectAtIndex:0];
    NSString *soundFilePath = [docsDir stringByAppendingPathComponent:kSound3];
     iview.image = [UIImage imageWithData:[[NSUserDefaults standardUserDefaults]objectForKey:kPhoto3]];
    NSURL *soundFileURL = [NSURL fileURLWithPath:soundFilePath];
    
    NSDictionary *recordSettings = [NSDictionary
                                    dictionaryWithObjectsAndKeys:
                                    [NSNumber numberWithInt:AVAudioQualityMin],
                                    AVEncoderAudioQualityKey,
                                    [NSNumber numberWithInt:16],
                                    AVEncoderBitRateKey,
                                    [NSNumber numberWithInt: 2],
                                    AVNumberOfChannelsKey,
                                    [NSNumber numberWithFloat:44100.0],
                                    AVSampleRateKey,
                                    nil];
    
    NSError *error = nil;
    
    audioRecorder = [[AVAudioRecorder alloc]
                     initWithURL:soundFileURL
                     settings:recordSettings
                     error:&error];
    
 
        testfield.text = testitout;
    }
-(IBAction)fourbuttonclick:(id)sender{
    testitout= @"4";
    [onebutton setBackgroundColor:[UIColor cyanColor]];
    [twobutton setBackgroundColor:[UIColor cyanColor]];
    [threebutton setBackgroundColor:[UIColor cyanColor]];
    [fourbutton setBackgroundColor:[UIColor redColor]];
    [fivebutton setBackgroundColor:[UIColor cyanColor]];
    [sixbutton setBackgroundColor:[UIColor cyanColor]];
    playbutton.enabled = YES;
    stopbutton.enabled = YES;
    
    
    
    dirPaths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    docsDir = [dirPaths objectAtIndex:0];
    NSString *soundFilePath = [docsDir stringByAppendingPathComponent:kSound4];
    
    NSURL *soundFileURL = [NSURL fileURLWithPath:soundFilePath];
     iview.image = [UIImage imageWithData:[[NSUserDefaults standardUserDefaults]objectForKey:kPhoto4]];
    NSDictionary *recordSettings = [NSDictionary
                                    dictionaryWithObjectsAndKeys:
                                    [NSNumber numberWithInt:AVAudioQualityMin],
                                    AVEncoderAudioQualityKey,
                                    [NSNumber numberWithInt:16],
                                    AVEncoderBitRateKey,
                                    [NSNumber numberWithInt: 2],
                                    AVNumberOfChannelsKey,
                                    [NSNumber numberWithFloat:44100.0],
                                    AVSampleRateKey,
                                    nil];
    
    NSError *error = nil;
    
    audioRecorder = [[AVAudioRecorder alloc]
                     initWithURL:soundFileURL
                     settings:recordSettings
                     error:&error];
    
    
          testfield.text = testitout;
    
}
-(IBAction)fivebuttonclick:(id)sender {
    testitout = @"5";
    [onebutton setBackgroundColor:[UIColor cyanColor]];
    [twobutton setBackgroundColor:[UIColor cyanColor]];
    [threebutton setBackgroundColor:[UIColor cyanColor]];
    [fourbutton setBackgroundColor:[UIColor cyanColor]];
    [fivebutton setBackgroundColor:[UIColor redColor]];
    [sixbutton setBackgroundColor:[UIColor cyanColor]];
    playbutton.enabled = YES;
    stopbutton.enabled = YES;
    
       
    dirPaths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    docsDir = [dirPaths objectAtIndex:0];
    NSString *soundFilePath = [docsDir stringByAppendingPathComponent:kSound5];
    
    NSURL *soundFileURL = [NSURL fileURLWithPath:soundFilePath];
     iview.image = [UIImage imageWithData:[[NSUserDefaults standardUserDefaults]objectForKey:kPhoto5]];
    NSDictionary *recordSettings = [NSDictionary
                                    dictionaryWithObjectsAndKeys:
                                    [NSNumber numberWithInt:AVAudioQualityMin],
                                    AVEncoderAudioQualityKey,
                                    [NSNumber numberWithInt:16],
                                    AVEncoderBitRateKey,
                                    [NSNumber numberWithInt: 2],
                                    AVNumberOfChannelsKey,
                                    [NSNumber numberWithFloat:44100.0],
                                    AVSampleRateKey,
                                    nil];
    
    NSError *error = nil;
    
    audioRecorder = [[AVAudioRecorder alloc]
                     initWithURL:soundFileURL
                     settings:recordSettings
                     error:&error];
    
  

  
        testfield.text = testitout;
}
-(IBAction)sixbuttonclick:(id)sender{
    
    testitout = @"6";
    [onebutton setBackgroundColor:[UIColor cyanColor]];
    [twobutton setBackgroundColor:[UIColor cyanColor]];
    [threebutton setBackgroundColor:[UIColor cyanColor]];
    [fourbutton setBackgroundColor:[UIColor cyanColor]];
    [fivebutton setBackgroundColor:[UIColor cyanColor]];
    [sixbutton setBackgroundColor:[UIColor redColor]];

    testfield.text = testitout;
    playbutton.enabled = YES;
    stopbutton.enabled = YES;
       
    dirPaths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    docsDir = [dirPaths objectAtIndex:0];
    NSString *soundFilePath = [docsDir stringByAppendingPathComponent:kSound6];
     iview.image = [UIImage imageWithData:[[NSUserDefaults standardUserDefaults]objectForKey:kPhoto6]];
    
    NSURL *soundFileURL = [NSURL fileURLWithPath:soundFilePath];
    
    NSDictionary *recordSettings = [NSDictionary
                                    dictionaryWithObjectsAndKeys:
                                    [NSNumber numberWithInt:AVAudioQualityMin],
                                    AVEncoderAudioQualityKey,
                                    [NSNumber numberWithInt:16],
                                    AVEncoderBitRateKey,
                                    [NSNumber numberWithInt: 2],
                                    AVNumberOfChannelsKey,
                                    [NSNumber numberWithFloat:44100.0],
                                    AVSampleRateKey,
                                    nil];
    
    NSError *error = nil;
    
    audioRecorder = [[AVAudioRecorder alloc]
                     initWithURL:soundFileURL
                     settings:recordSettings
                     error:&error];
    
  
    
}




-(void)audioPlayerDidFinishPlaying:
(AVAudioPlayer *)player successfully:(BOOL)flag
{
    recordbutton.enabled = YES;
    stopbutton.enabled = YES;
}
-(void)audioPlayerDecodeErrorDidOccur:
(AVAudioPlayer *)player
                                error:(NSError *)error
{
    NSLog(@"Decode Error occurred");
}
-(void)audioRecorderDidFinishRecording:
(AVAudioRecorder *)recorder
                          successfully:(BOOL)flag
{
}
-(void)audioRecorderEncodeErrorDidOccur:
(AVAudioRecorder *)recorder
                                  error:(NSError *)error
{
    NSLog(@"Encode Error occurred");
}


- (void)viewDidUnload {
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
    audioPlayer = nil;
    audioRecorder = nil;
    stopbutton = nil;
    recordbutton = nil;
    playbutton = nil;
}

@end
