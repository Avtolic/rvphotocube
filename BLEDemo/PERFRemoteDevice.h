//
//  PERFRemoteDevice.h
//  BLEDemo
//
//  Created by Tomas Henriksson on 1/27/12.
//  Copyright (c) 2012 connectBlue. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "SerialPort.h"
#import <CoreBluetooth/CBPeripheral.h>

@interface PERFRemoteDevice : NSObject

@property (nonatomic, strong) SerialPort    *serialPort;
@property (nonatomic, weak) UISwitch        *txSwitch;
@property (nonatomic, strong) NSTimer       *timer;

@property (nonatomic) BOOL                  txSwitchOn;

@property (nonatomic) NSUInteger            txBytes;
@property (nonatomic) NSUInteger            txBitrate;
@property (nonatomic) NSUInteger            rxBytes;
@property (nonatomic) NSUInteger            rxBitrate;
@property (nonatomic) UInt8                 rxSequence;
@property (nonatomic) NSUInteger            rxErrors;
@property (nonatomic) BOOL                  rxWaitingFirst;

@property (nonatomic, strong) NSData        *data;

- (PERFRemoteDevice*) initWithPort: (SerialPort*) serialPort andDataLen: (NSInteger)dataLen;
- (void) formatData: (bool) reset;

@end
