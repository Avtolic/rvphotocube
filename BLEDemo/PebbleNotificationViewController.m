//
//  PebbleNotificationViewController.m
//  RVPhotoCube
//
//  Created by Steven Skindell on 2/24/14.
//  Copyright (c) 2014 skindellmagic. All rights reserved.
//

#import "PebbleNotificationViewController.h"

#define knot1 @"knot1"
#define knot2 @"knot2"
#define knot3 @"knot3"
#define knot4 @"knot4"
#define knot5 @"knot5"
#define knot6 @"knot6"

#define kPhoto1 @"Photo1"
#define kPhoto2 @"Photo2"
#define kPhoto3 @"Photo3"
#define kPhoto4 @"Photo4"
#define kPhoto5 @"Photo5"
#define kPhoto6 @"Photo6"

static NSArray *dirPaths;
static NSString *docsDir;




@interface PebbleNotificationViewController ()

@end

@implementation PebbleNotificationViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}
@synthesize onebutton;
@synthesize twobutton;
@synthesize threebutton;
@synthesize fourbutton;
@synthesize fivebutton;
@synthesize sixbutton;
@synthesize savebutton;
@synthesize saveallbutton;


@synthesize testfield1;
@synthesize testfield2;
@synthesize testfield3;
@synthesize testfield4;
@synthesize testfield5;
@synthesize testfield6;

@synthesize iview;

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
   
    testfield1.text =[defaults objectForKey:@"knot1"];
    testfield2.text =[defaults objectForKey:@"knot2"];
    testfield3.text =[defaults objectForKey:@"knot3"];
    testfield4.text =[defaults objectForKey:@"knot4"];
    testfield5.text =[defaults objectForKey:@"knot5"];
    testfield6.text =[defaults objectForKey:@"knot6"];
    
    
    
	// Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


-(IBAction)sixbuttonclick:(id)sender{
    [onebutton setBackgroundColor:[UIColor cyanColor]];
    [twobutton setBackgroundColor:[UIColor cyanColor]];
    [threebutton setBackgroundColor:[UIColor cyanColor]];
    [fourbutton setBackgroundColor:[UIColor cyanColor]];
    [fivebutton setBackgroundColor:[UIColor cyanColor]];
    [sixbutton setBackgroundColor:[UIColor redColor]];
    dirPaths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    docsDir = [dirPaths objectAtIndex:0];
    iview.image = [UIImage imageWithData:[[NSUserDefaults standardUserDefaults]objectForKey:kPhoto6]];
    [testfield6 becomeFirstResponder];

   }

-(IBAction)fivebuttonclick:(id)sender{
    [onebutton setBackgroundColor:[UIColor cyanColor]];
    [twobutton setBackgroundColor:[UIColor cyanColor]];
    [threebutton setBackgroundColor:[UIColor cyanColor]];
    [fourbutton setBackgroundColor:[UIColor cyanColor]];
    [fivebutton setBackgroundColor:[UIColor redColor]];
    [sixbutton setBackgroundColor:[UIColor cyanColor]];
    dirPaths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    docsDir = [dirPaths objectAtIndex:0];
   
    iview.image = [UIImage imageWithData:[[NSUserDefaults standardUserDefaults]objectForKey:kPhoto5]];
       [testfield5 becomeFirstResponder];
 
}

-(IBAction)fourbuttonclick:(id)sender{
    [onebutton setBackgroundColor:[UIColor cyanColor]];
    [twobutton setBackgroundColor:[UIColor cyanColor]];
    [threebutton setBackgroundColor:[UIColor cyanColor]];
    [fourbutton setBackgroundColor:[UIColor redColor]];
    [fivebutton setBackgroundColor:[UIColor cyanColor]];
    [sixbutton setBackgroundColor:[UIColor cyanColor]];
    dirPaths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    docsDir = [dirPaths objectAtIndex:0];
   
    iview.image = [UIImage imageWithData:[[NSUserDefaults standardUserDefaults]objectForKey:kPhoto4]];
       [testfield4 becomeFirstResponder];
  
}

-(IBAction)threebuttonclick:(id)sender{
    [onebutton setBackgroundColor:[UIColor cyanColor]];
    [twobutton setBackgroundColor:[UIColor cyanColor]];
    [threebutton setBackgroundColor:[UIColor redColor]];
    [fourbutton setBackgroundColor:[UIColor cyanColor]];
    [fivebutton setBackgroundColor:[UIColor cyanColor]];
    [sixbutton setBackgroundColor:[UIColor cyanColor]];
    dirPaths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    docsDir = [dirPaths objectAtIndex:0];
   
    iview.image = [UIImage imageWithData:[[NSUserDefaults standardUserDefaults]objectForKey:kPhoto3]];
       [testfield3 becomeFirstResponder];
  
}

-(IBAction)twobuttonclick:(id)sender{
    [onebutton setBackgroundColor:[UIColor cyanColor]];
    [twobutton setBackgroundColor:[UIColor redColor]];
    [threebutton setBackgroundColor:[UIColor cyanColor]];
    [fourbutton setBackgroundColor:[UIColor cyanColor]];
    [fivebutton setBackgroundColor:[UIColor cyanColor]];
    [sixbutton setBackgroundColor:[UIColor cyanColor]];
    dirPaths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    docsDir = [dirPaths objectAtIndex:0];
   
    iview.image = [UIImage imageWithData:[[NSUserDefaults standardUserDefaults]objectForKey:kPhoto2]];
       [testfield2 becomeFirstResponder];
 
}

-(IBAction)onebuttonclick:(id)sender{
    [onebutton setBackgroundColor:[UIColor redColor]];
    [twobutton setBackgroundColor:[UIColor cyanColor]];
    [threebutton setBackgroundColor:[UIColor cyanColor]];
    [fourbutton setBackgroundColor:[UIColor cyanColor]];
    [fivebutton setBackgroundColor:[UIColor cyanColor]];
    [sixbutton setBackgroundColor:[UIColor cyanColor]];
    dirPaths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    docsDir = [dirPaths objectAtIndex:0];
   
    iview.image = [UIImage imageWithData:[[NSUserDefaults standardUserDefaults]objectForKey:kPhoto1]];
       [testfield1 becomeFirstResponder];
 
}
-(IBAction)savebuttonclick:(id)sender
{
    NSString *one = [testfield1 text];
    NSString *two = [testfield2 text];
    NSString *three = [testfield3 text];
    NSString *four = [testfield4 text];
    NSString *five = [testfield5 text];
    NSString *six = [testfield6 text];
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
      [defaults setObject:one forKey:@"knot1"];
      [defaults setObject:two forKey:@"knot2"];
      [defaults setObject:three forKey:@"knot3"];
      [defaults setObject:four forKey:@"knot4"];
      [defaults setObject:five forKey:@"knot5"];
      [defaults setObject:six forKey:@"knot6"];
    [defaults synchronize];
    
    
}


-(IBAction)saveallbuttonclick:(id)sender
{
    NSString *one = [testfield1 text];
    NSString *two = [testfield2 text];
    NSString *three = [testfield3 text];
    NSString *four = [testfield4 text];
    NSString *five = [testfield5 text];
    NSString *six = [testfield6 text];
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults setObject:one forKey:@"knot1"];
    [defaults setObject:two forKey:@"knot2"];
    [defaults setObject:three forKey:@"knot3"];
    [defaults setObject:four forKey:@"knot4"];
    [defaults setObject:five forKey:@"knot5"];
    [defaults setObject:six forKey:@"knot6"];
    [defaults synchronize];
    
    
}

@end
