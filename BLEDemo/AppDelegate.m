
#import "AppDelegate.h"
#import "ScanTableViewController.h"
#import "ServiceTableViewController.h"
#import "ChatTableViewController.h"
#import "DemoViewController.h"
#import "ImageCache.h"

#define SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(v)  ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] != NSOrderedAscending)

@implementation AppDelegate
{
    ScanTableViewController* scanTableViewController;
    ServiceTableViewController* serviceTableViewController;
    DemoViewController* demoViewController;
    ChatTableViewController* chatTableViewController;
    
    NSMutableArray* discoveredPeripherals;
    
}

@synthesize window = _window;

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    // Override point for customization after application launch.
    [self registerForRemoteNotification];
    
    UITabBarController *tabBarController = (UITabBarController *)self.window.rootViewController;
    
    UINavigationController *navigationController;
    
    navigationController = [[tabBarController viewControllers] objectAtIndex:0];
	scanTableViewController = [[navigationController viewControllers] objectAtIndex:0];
    
    navigationController = [[tabBarController viewControllers] objectAtIndex:1];
    serviceTableViewController = [[navigationController viewControllers] objectAtIndex:0];
    
    navigationController = [[tabBarController viewControllers] objectAtIndex:2];
    demoViewController = [[navigationController viewControllers] objectAtIndex:0];
    
    navigationController = [[tabBarController viewControllers] objectAtIndex:3];
    chatTableViewController = [[navigationController viewControllers] objectAtIndex:0];
    
    discoveredPeripherals = [[NSMutableArray alloc] init];
    
    [scanTableViewController initWithPeripherals:discoveredPeripherals];
    [serviceTableViewController initWithPeripherals:discoveredPeripherals];
    [demoViewController initWithPeripherals:discoveredPeripherals];
    [chatTableViewController initWithPeripherals:discoveredPeripherals];
    
    
    application.applicationIconBadgeNumber = 0;
    UILocalNotification *localNotif = [launchOptions objectForKey:UIApplicationLaunchOptionsLocalNotificationKey];
    
    if (localNotif)
    {
        NSLog(@"Recieved Notification %@",localNotif);
    }
    
    return YES;
}

- (void)registerForRemoteNotification {
    if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"8.0")) {
        UIUserNotificationType types = UIUserNotificationTypeSound | UIUserNotificationTypeBadge | UIUserNotificationTypeAlert;
        UIUserNotificationSettings *notificationSettings = [UIUserNotificationSettings settingsForTypes:types categories:nil];
        [[UIApplication sharedApplication] registerUserNotificationSettings:notificationSettings];
    } else {
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Wdeprecated-declarations"
        [[UIApplication sharedApplication] registerForRemoteNotificationTypes:(UIRemoteNotificationTypeBadge | UIRemoteNotificationTypeSound | UIRemoteNotificationTypeAlert)];
#pragma clang diagnostic pop
    }
}

#ifdef __IPHONE_8_0
- (void)application:(UIApplication *)application didRegisterUserNotificationSettings:(UIUserNotificationSettings *)notificationSettings {
    [application registerForRemoteNotifications];
}
#endif

- (void)application:(UIApplication *)app didReceiveLocalNotification:(UILocalNotification *)notif
{
	// Handle the notificaton when the app is running
	NSLog(@"Recieved Notification %@",notif);
}

- (void)applicationWillResignActive:(UIApplication *)application
{
    /*
     Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
     Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
     */
}

- (void)applicationDidEnterBackground:(UIApplication *)application
{
    /*
     Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
     If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
     */
    
    [scanTableViewController enterBackground];
}

- (void)applicationWillEnterForeground:(UIApplication *)application
{
    /*
     Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
     */
}

- (void)applicationDidBecomeActive:(UIApplication *)application
{
    /*
     Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
     */
    
    [scanTableViewController enterForeground];
}

- (void)applicationWillTerminate:(UIApplication *)application
{
    /*
     Called when the application is about to terminate.
     Save data if appropriate.
     See also applicationDidEnterBackground:.
     */
    
    discoveredPeripherals = nil;
}




@end
