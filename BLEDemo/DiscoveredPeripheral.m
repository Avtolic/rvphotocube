//
//  DiscoveredPeripheral.m
//  BLEDemo

#import "DiscoveredPeripheral.h"

@implementation DiscoveredPeripheral

@synthesize peripheral;
@synthesize advertisment;
@synthesize rssi;
@synthesize state;

- (DiscoveredPeripheral*) initWithPeripheral: (CBPeripheral*) newPeripheral andAdvertisment: (NSDictionary*) newAdvertisment andRssi: (NSNumber*) newRssi
{
    self.peripheral = newPeripheral;
    self.advertisment = newAdvertisment;
    self.rssi = newRssi;
    self.state = DP_STATE_IDLE;
    
    return self;
}
@end
