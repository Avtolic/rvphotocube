//
//  ServiceTableViewController.m
//  BLEDemo


#import "ServiceTableViewController.h"
#import "DiscoveredPeripheral.h"
#import <CoreBluetooth/CoreBluetooth.h>
//#import "EditValueTableViewController.h"
#import "CharactTableViewController.h"
#import "SerialPort.h"
#import "BLEDefinitions.h"

typedef enum
{
    SERVICE_S_NOT_LOADED,
    SERVICE_S_DISAPPEARED,
    
    SERVICE_S_APPEARED_IDLE,
    SERVICE_S_APPEARED_WAIT_SERVICES,
    SERVICE_S_APPEARED_NO_CONNECT_PERIPH
    
} SERVICE_State;


@implementation ServiceTableViewController
{
    SERVICE_State   state;
    
    NSMutableArray          *discoveredPeripherals;
    DiscoveredPeripheral    *connectedPeripheral;
    
    NSInteger       nCharacteristics;
    NSInteger       nSections;
    NSInteger       nRows;
    
    //EditValueTableViewController *editValueTableViewController;
    CharactTableViewController *charactTableViewController;
}

static CBCharacteristic* characteristicFromSection(CBPeripheral *peripheral, NSInteger section, CBService** pService);

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (IBAction)serviceRefresh:(id)sender
{
    [self refresh];
}

- (void) initWithPeripherals: (NSMutableArray*) dp
{
    discoveredPeripherals = dp;
    
    //connectedPeripherals = [[NSMutableArray alloc] init];
    connectedPeripheral = nil;
    
    state = SERVICE_S_NOT_LOADED;
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle

/*
- (void) pollTimer: (NSTimer*) timer
{
    if(self.peripheral.isConnected == TRUE)
    {
        [self.peripheral discoverServices:nil];
    }
    else
    {
        [NSTimer scheduledTimerWithTimeInterval:1 target:self selector: @selector(pollTimer:) userInfo:nil repeats:NO];
    }
}
 */

- (void)viewDidLoad
{
    [super viewDidLoad];

    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
 
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
    
    state = SERVICE_S_DISAPPEARED;
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
    
    discoveredPeripherals = nil;
    connectedPeripheral = nil;
    
    state = SERVICE_S_NOT_LOADED;
}

- (void)viewWillAppear:(BOOL)animated
{    
    [super viewWillAppear:animated];
}

- (void)viewDidAppear:(BOOL)animated
{
    DiscoveredPeripheral *dp;
    
    [super viewDidAppear:animated];
    
    // Clear View
    //[connectedPeripherals removeAllObjects];
    connectedPeripheral = nil;
    
    // Update Connected Devices Array
    for(int i = 0; (i < discoveredPeripherals.count) && (connectedPeripheral == nil); i++)
    {
        dp = [discoveredPeripherals objectAtIndex:i];
        
        if ((dp.state == DP_STATE_CONNECTED) && (dp.peripheral.state == CBPeripheralStateConnected))
        {
            //[connectedPeripherals addObject:dp];
            connectedPeripheral = dp;
        }
    }
    
    // Calculate Characteristics and set nSections
    nSections = 0;
    
    if(connectedPeripheral != nil)
    {
        //dp = [connectedPeripherals objectAtIndex:0];
            
        connectedPeripheral.peripheral.delegate = self;
        
        /*
        for(int i = 0; i < connectedPeripheral.peripheral.services.count; i++)
        {
            nSections += [[[connectedPeripheral.peripheral.services objectAtIndex:i] characteristics] count];
        }
         */
            
        state = SERVICE_S_APPEARED_WAIT_SERVICES;
                
        [self refresh];
    }
    else
    {        
        state = SERVICE_S_APPEARED_NO_CONNECT_PERIPH;
    }
        
    [self.tableView reloadData];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    
    if(connectedPeripheral != nil)
    {
        connectedPeripheral.peripheral.delegate = nil;
    }
}

- (void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];    
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    NSInteger n = 0;
    
    if(connectedPeripheral != nil)
    {
        n = connectedPeripheral.peripheral.services.count;
    }
    
    return n;
}

- (NSString*) tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    NSString *str = nil;
    
    switch(section)
    {
        case 0:
            if(connectedPeripheral != nil)
                str = connectedPeripheral.peripheral.name;
            else
                str = @"No Connected Peripheral";
            break;
            
        default:
            break;
    }
    
    return str;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"ServiceCell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    cell.textLabel.text = nil;
    //cell.accessoryType = UITableViewCellAccessoryNone;
    //cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    // Configure the cell...

    if(connectedPeripheral.peripheral.services.count > indexPath.row)
    {
        CBService*  service;
        
        service = [connectedPeripheral.peripheral.services objectAtIndex:indexPath.row];
        
        cell.textLabel.text = strFromServiceUUID(service.UUID);

        
        /*if(dp.peripheral.services.count > 0)
        {
            CBService*          service;
            
            service = [dp.peripheral.services objectAtIndex:indexPath.row];
            
            cell.textLabel.text = strFromServiceUUID(service.UUID);
            
       
            BOOL                done = FALSE;
            NSInteger           cnt = 0;
            NSInteger           index;
            CBCharacteristic*   ch;
                    
            for(int i = 0; (i < dp.peripheral.services.count) && (done == FALSE); i++)
            {
                service = [dp.peripheral.services objectAtIndex:i];
                        
                index = indexPath.section - cnt;
                        
                if(index < service.characteristics.count)
                {                            
                    ch = [service.characteristics objectAtIndex:index];
                            
                    switch(indexPath.row)
                    {
                        case 0:
                            //cell.textLabel.text = [[NSString alloc] initWithFormat:@"%@", ch.service.UUID];
                            cell.textLabel.text = strFromServiceUUID(ch.service.UUID);
                            break;
                                    
                        case 1:
                            //cell.textLabel.text = [[NSString alloc] initWithFormat:@"%@", ch.UUID];
                            cell.textLabel.text = strFromCharacteristicUUID(ch.service.UUID, ch.UUID);
                            break;
                                    
                        case 2:
                            //cell.accessoryType = UITableViewCellAccessoryNone;
                            cell.textLabel.text = strFromCharacteristicValue(ch.service.UUID, ch.UUID, ch.value);
                                    
                            //[[NSString alloc] initWithFormat:@"%s", [ch.value description]];
                            break;
                                    
                        case 3:
                            cell.textLabel.text = strFromCharacteristicProperties(ch.properties);
                            break;
                                    
                        case 4:
                            if(ch.isNotifying == TRUE)
                            {
                                cell.textLabel.text = @"Is Notifying";
                            }
                            else
                            {
                                cell.textLabel.text = @"Is Not Notifying";         
                            }
                            break;
                                    
                        case 5:
                            if(ch.isBroadcasted == TRUE)
                            {
                                cell.textLabel.text = @"Is Broadcasted";  
                            }
                            else
                            {
                                cell.textLabel.text = @"Is Not Broadcasted";
                            }
                            break;
                                    
                        default:
                            break;
                    }
                            
                    done = TRUE;
                }
                else
                {
                    cnt += service.characteristics.count;
                }
            }
        }
         */
    }

    //DiscoveredPeripheral* dp = [connectedPeripherals objectAtIndex:indexPath.section];
    
    //cell.textLabel.text = [dp.peripheral.services objectAtIndex:indexPath.row];
    
    return cell;
}

/*
- (NSString*) tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    NSString *str;
    
    switch(section)
    {
        case 0:
            str = @"Characteristics";
            break;
            
        default:
            break;
    }
    
    return str;
}
 */

/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationFade];
    }   
    else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath
{
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Navigation logic may go here. Create and push another view controller.
    /*
     <#DetailViewController#> *detailViewController = [[<#DetailViewController#> alloc] initWithNibName:@"<#Nib name#>" bundle:nil];
     // ...
     // Pass the selected object to the new view controller.
     [self.navigationController pushViewController:detailViewController animated:YES];
     */
    
    /*
    CBService* service;
    
    //selectedSection = indexPath.section;
    
    editValueTableViewController.peripheral = connectedPeripheral.peripheral;
    
    editValueTableViewController.characteristic =  characteristicFromSection(editValueTableViewController.peripheral, indexPath.section, &service);
    
    editValueTableViewController.service = service;
    
    editValueTableViewController.strServiceUuid = strFromServiceUUID(service.UUID);
    editValueTableViewController.strCharactUuid = strFromCharacteristicUUID(service.UUID, editValueTableViewController.characteristic.UUID);
     */
    
    charactTableViewController.peripheral = connectedPeripheral.peripheral;
    charactTableViewController.service = [connectedPeripheral.peripheral.services objectAtIndex:indexPath.row];
    
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}

static CBCharacteristic* characteristicFromSection(CBPeripheral *peripheral, NSInteger section, CBService** pService)
{
    CBCharacteristic *characteristic = nil;
    CBService* service;
    NSInteger n;
    
    for(int i = 0; (i < peripheral.services.count) && (characteristic == nil); i++)
    {
        service = [peripheral.services objectAtIndex:i];
        
        n = [[service characteristics] count];
        
        if(section < n)
        {
            characteristic = [[service characteristics] objectAtIndex:section];
            
            *pService = service;
        }
        else
        {
            section -= n;
        }
    }
    
    return characteristic;
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
     if ([segue.identifier isEqualToString:@"CharactSegue"])
     {
         //editValueTableViewController = segue.destinationViewController;
         charactTableViewController = segue.destinationViewController;
     
         //ServiceTableViewController *serviceTableViewController = [[navigationController viewControllers] objectAtIndex:0];
     }
}

#pragma mark - CBPeripheralDelegate

- (void)peripheral:(CBPeripheral *)peripheral didDiscoverServices:(NSError *)error
{
    /*
    indexService = 0;
    
    [peripheral discoverCharacteristics:nil forService:[peripheral.services objectAtIndex:indexService]];
     */
   
    /*
    for(int i = 0; i < peripheral.services.count; i++)
    {
        [peripheral discoverCharacteristics:nil forService:[peripheral.services objectAtIndex:i]];
    }
     */
    
    state = SERVICE_S_APPEARED_IDLE;
    
    [self.tableView reloadData];
}

- (void)peripheral:(CBPeripheral *)peripheral didDiscoverCharacteristicsForService:(CBService *)service error:(NSError *)error
{
    CBCharacteristic* charact;
    /*
    indexCharacteristic = 0;

    [peripheral readValueForCharacteristic:[service.characteristics objectAtIndex:indexCharacteristic]];
     */
    
    for(int i = 0; i < service.characteristics.count; i++)
    {
        nSections++;
        
        charact = [service.characteristics objectAtIndex:i];
        
        /*
        if( ((charact.properties & CBCharacteristicPropertyNotify) != 0) &&
            (charact.isNotifying == FALSE))
        {
            [peripheral setNotifyValue: TRUE forCharacteristic:charact];
        }
        */
        
        [peripheral readValueForCharacteristic: charact];
    }
}

- (void)peripheral:(CBPeripheral *)peripheral didUpdateValueForCharacteristic:(CBCharacteristic *)characteristic error:(NSError *)error
{
    //CBService* service = [peripheral.services objectAtIndex:indexService];
    
    [self.tableView reloadData];
    
    /*
    indexCharacteristic++;
    
    if(indexCharacteristic < service.characteristics.count)
    {
        [peripheral readValueForCharacteristic:[service.characteristics objectAtIndex:indexCharacteristic]];
    }
    else
    {
        indexService++;
        
        if(indexService < peripheral.services.count)
        {
            [peripheral discoverCharacteristics:nil forService:[peripheral.services objectAtIndex:indexService]];
        }
        else
        {
            //[self.tableView reloadData];
        }
    }
    
    if(error != nil)
    {
        NSLog(@"ERROR LOG: %@", error);
    }
     */
    
    //[self.tableView reloadData];
}

- (void)peripheral:(CBPeripheral *)peripheral didDiscoverDescriptorsForCharacteristic:(CBCharacteristic *)characteristic error:(NSError *)error
{
    
}

- (void)peripheral:(CBPeripheral *)peripheral didDiscoverIncludedServicesForService:(CBService *)service error:(NSError *)error
{
    
}

- (void)peripheral:(CBPeripheral *)peripheral didUpdateNotificationStateForCharacteristic:(CBCharacteristic *)characteristic error:(NSError *)error
{
    [self.tableView reloadData];
}

- (void)peripheral:(CBPeripheral *)peripheral didUpdateValueForDescriptor:(CBDescriptor *)descriptor error:(NSError *)error
{
    
}

- (void)peripheral:(CBPeripheral *)peripheral didWriteValueForCharacteristic:(CBCharacteristic *)characteristic error:(NSError *)error
{
    
}

- (void)peripheral:(CBPeripheral *)peripheral didWriteValueForDescriptor:(CBDescriptor *)descriptor error:(NSError *)error
{
    
}

- (void)peripheralDidUpdateRSSI:(CBPeripheral *)peripheral error:(NSError *)error
{
    
}

- (void) refresh
{
    DiscoveredPeripheral* dp;
    
    nSections = 0;
    nRows = 4;//6;
    
    connectedPeripheral = nil;
    
    for(int i = 0; (i < discoveredPeripherals.count) && (connectedPeripheral == nil); i++)
    {
        dp = [discoveredPeripherals objectAtIndex:i];
        
        if ((dp.state == DP_STATE_CONNECTED) && (dp.peripheral.state == CBPeripheralStateConnected))
        {
            connectedPeripheral = dp;
        }
    }

    if(connectedPeripheral != nil)
    {
        connectedPeripheral.peripheral.delegate = self;
            
        [connectedPeripheral.peripheral discoverServices:nil];
    }
    
    [self.tableView reloadData];
}

@end
