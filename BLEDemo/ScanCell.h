//
//  ScanCell.h
//  BLEDemo

#import <UIKit/UIKit.h>

@interface ScanCell : UITableViewCell

@property (nonatomic, strong) IBOutlet UILabel* labelName;
@property (nonatomic, strong) IBOutlet UILabel* labelInfo;
@property (nonatomic, strong) IBOutlet UIActivityIndicatorView* activityView;

@end
