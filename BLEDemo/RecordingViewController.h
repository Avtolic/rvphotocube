
#import <UIKit/UIKit.h>
#import <AVFoundation/AVFoundation.h>

@interface RecordingViewController : UIViewController <AVAudioRecorderDelegate, AVAudioPlayerDelegate>
{
    AVAudioRecorder *audioRecorder;
    AVAudioPlayer *audioPlayer;
    UIButton *playbutton;
    UIButton *recordbutton;
    UIButton *stopbutton;
    UIImageView *iview;
    
}


@property (strong,nonatomic) IBOutlet UIButton *recordbutton;
@property (strong,nonatomic) IBOutlet UIButton *savebutton;
@property (strong,nonatomic) IBOutlet UIButton *playbutton;
@property (strong,nonatomic) IBOutlet UIButton *stopbutton;
@property (strong,nonatomic) IBOutlet UIButton *onebutton;
@property (strong,nonatomic) IBOutlet UIButton *twobutton;
@property (strong,nonatomic) IBOutlet UIButton *threebutton;
@property (strong,nonatomic) IBOutlet UIButton *fourbutton;
@property (strong,nonatomic) IBOutlet UIButton *fivebutton;
@property (strong,nonatomic) IBOutlet UIButton *sixbutton;
@property (strong, nonatomic) IBOutlet UITextField *testfield;
@property (strong, nonatomic) IBOutlet UIImageView *iview;



- (IBAction)recordbuttonclick:(id)sender;
- (IBAction)savebuttonclick:(id)sender;
- (IBAction)playbuttonclick:(id)sender;
- (IBAction)stopbuttonclick:(id)sender;

-(IBAction)onebuttonclick:(id)sender;
-(IBAction)twobuttonclick:(id)sender;
-(IBAction)threebuttonclick:(id)sender;
-(IBAction)fourbuttonclick:(id)sender;
-(IBAction)fivebuttonclick:(id)sender;
-(IBAction)sixbuttonclick:(id)sender;




@end
