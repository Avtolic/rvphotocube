//
//  EditValueTableViewController.m
//  BLEDemo

#import "EditValueTableViewController.h"

enum SelectedValue
{
    SV_NONE,
    SV_TEXT_TEXT_FIELD,
    SV_HEX_TEXT_FIELD,
    SV_INT_TEXT_FIELD
    
};


@implementation EditValueTableViewController
{
    enum SelectedValue selectedValue;
}

@synthesize peripheral;
@synthesize service;
@synthesize characteristic;
@synthesize strServiceUuid;
@synthesize strCharactUuid;

@synthesize textValueTextField;
@synthesize hexValueTextField;
@synthesize intValueTextField;
@synthesize suuidLabel;
@synthesize cuuidLabel;
@synthesize cValueLabel;

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];

    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
 
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
}

- (void)viewDidUnload
{
    [self setTextValueTextField:nil];
    [self setHexValueTextField:nil];
    [self setSuuidLabel:nil];
    [self setCuuidLabel:nil];
    [self setCValueLabel:nil];
    [self setIntValueTextField:nil];
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    peripheral.delegate = self;
    
    textValueTextField.delegate = self;
    hexValueTextField.delegate = self;
    intValueTextField.delegate = self;
    
    [self selectTextField:nil];
    
    //suuidLabel.text = [[NSString alloc] initWithFormat:@"%@", service.UUID];
    //cuuidLabel.text = [[NSString alloc] initWithFormat:@"%@", characteristic.UUID];
    suuidLabel.text = strServiceUuid;
    cuuidLabel.text = strCharactUuid;
    cValueLabel.text = [[NSString alloc] initWithFormat:@"%@", characteristic.value];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    
    if(peripheral != nil)
        peripheral = nil;
}

- (void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

#pragma mark - Table view data source

/*
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return 2;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    return 3;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier;
    
    switch(indexPath.section)
    {
        case 0:
            CellIdentifier = @"EditValueTextCell";
            break;
            
        default:
            CellIdentifier = @"EditValueBasicCell";
            break;
    }
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    // Configure the cell...
    
    return cell;
}
 */

/*
- (NSString*) tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    NSString *str;
    
    switch(section)
    {
        case 0:
            str = @"Edit Value";
            break;
            
        case 1:
            str = @"Characteristic";
            break;
            
        default:
            break;
    }
    
    return str;
}
 */


/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationFade];
    }   
    else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath
{
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Navigation logic may go here. Create and push another view controller.
    /*
     <#DetailViewController#> *detailViewController = [[<#DetailViewController#> alloc] initWithNibName:@"<#Nib name#>" bundle:nil];
     // ...
     // Pass the selected object to the new view controller.
     [self.navigationController pushViewController:detailViewController animated:YES];
     */
    
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    if ( (indexPath.section == 0) && (indexPath.row == 0))
    {
		[self.textValueTextField becomeFirstResponder];
    }
    else if( (indexPath.section == 0) && (indexPath.row == 1))
    {
        [self.hexValueTextField becomeFirstResponder];
    }
    else if( (indexPath.section == 0) && (indexPath.row == 2))
    {
        [self.intValueTextField becomeFirstResponder];
    }
}

- (void) selectTextField: (UITextField*) textField
{
    UITableViewCell *cellText;
    UITableViewCell *cellHex;
    UITableViewCell *cellInt;
    NSIndexPath *indexPath;
    NSUInteger index[] = {0, 0};
    
    indexPath = [NSIndexPath indexPathWithIndexes:index length:2];
    cellText = [self.tableView cellForRowAtIndexPath:indexPath];
    
    index[1] = 1;
    
    indexPath = [NSIndexPath indexPathWithIndexes:index length:2];
    cellHex = [self.tableView cellForRowAtIndexPath:indexPath];
    
    index[1] = 2;
    
    indexPath = [NSIndexPath indexPathWithIndexes:index length:2];
    cellInt = [self.tableView cellForRowAtIndexPath:indexPath];
    
    if(textField == textValueTextField)
    {
        selectedValue = SV_TEXT_TEXT_FIELD;
        
        cellText.accessoryType = UITableViewCellAccessoryCheckmark;
        cellHex.accessoryType = UITableViewCellAccessoryNone;
        cellInt.accessoryType = UITableViewCellAccessoryNone;
    }
    else if(textField == hexValueTextField)
    {
        selectedValue = SV_HEX_TEXT_FIELD;
        
        cellText.accessoryType = UITableViewCellAccessoryNone;
        cellHex.accessoryType = UITableViewCellAccessoryCheckmark;
        cellInt.accessoryType = UITableViewCellAccessoryNone;
    }
    else if(textField == intValueTextField)
    {
        selectedValue = SV_INT_TEXT_FIELD;
        
        cellText.accessoryType = UITableViewCellAccessoryNone;
        cellHex.accessoryType = UITableViewCellAccessoryNone;
        cellInt.accessoryType = UITableViewCellAccessoryCheckmark;
    }
    else
    {
        selectedValue = SV_NONE;
        
        cellText.accessoryType = UITableViewCellAccessoryNone;
        cellHex.accessoryType = UITableViewCellAccessoryNone;
        cellInt.accessoryType = UITableViewCellAccessoryNone;
    }
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{    
    [self selectTextField:textField];
    
    [textField resignFirstResponder];
    
    return YES;
}

- (IBAction)setValue:(id)sender
{
    NSData *data = nil;
    
    if(selectedValue == SV_TEXT_TEXT_FIELD)
    {
        NSRange     range;
        BOOL        ok = TRUE;
        NSUInteger  len;
        
        range.location = 0;
        range.length = [textValueTextField.text lengthOfBytesUsingEncoding:NSUTF8StringEncoding];
        
        unsigned char   *buf = malloc(range.length);
        
        ok = [textValueTextField.text getBytes:buf maxLength:range.length usedLength:&len encoding:NSUTF8StringEncoding options:NSStringEncodingConversionAllowLossy range:range remainingRange:&range];
        
        if(ok == TRUE)
        {
            data = [NSData  dataWithBytes:buf length:len];
        }
        
        free(buf);

    }
    else if(selectedValue == SV_HEX_TEXT_FIELD)
    {
        NSScanner       *scanner = [NSScanner alloc];
        NSInteger       bufSize = (hexValueTextField.text.length + 1) / 2;
        unsigned char   *buf = malloc(bufSize);
        
        NSString    *str;
        NSRange     range;
        unsigned int  val;
        BOOL        ok = TRUE;
        NSInteger   pos = 0;
        
        for(int i = 0; (i < bufSize) && (ok == TRUE); i++)
        {
            range.location = pos;
            
            /*
            if(range.location + 1 < hexValueTextField.text.length)
                range.length = 2;
            else
                range.length = 1;
             */
            
            if( (pos == 0) && (hexValueTextField.text.length % 2) != 0)
            {
                range.length = 1;
                pos += 1;
            }
            else
            {
                range.length = 2;
                pos += 2;
            }
            
            str = [hexValueTextField.text substringWithRange:range];
            
            scanner = [scanner initWithString:str];
            
            ok = [scanner scanHexInt: &val];
            
            if(ok)
            {
                buf[bufSize - i - 1] = val;
            }
        }
        
        if(ok)
        {
            data = [NSData dataWithBytes:buf length:bufSize];
        }
        
        free(buf);
    }
    else if(selectedValue == SV_INT_TEXT_FIELD)
    {        
        NSScanner       *scanner = [NSScanner scannerWithString:intValueTextField.text];
        NSInteger       valInt;
        BOOL            ok;
        unsigned char   *buf;
        NSUInteger      bufSize;
     
        ok = [scanner scanInteger:&valInt];
        
        if(ok)
        {
            bufSize = characteristic.value.length;
            buf = malloc(bufSize);

            for(int i = 0; i < bufSize; i++)
            {
                buf[i] = (unsigned char)(valInt >> (i * 8));
            }
            
            data = [NSData dataWithBytes:buf length:bufSize];
            
            free(buf);
        }
    }

    if(data != nil)
    {
        if((characteristic.properties & 0x04) != 0)
        {
            [peripheral writeValue:data forCharacteristic:characteristic type:CBCharacteristicWriteWithoutResponse];
        }
        else if((characteristic.properties & 0x08) != 0)
        {
            [peripheral writeValue:data forCharacteristic:characteristic type:CBCharacteristicWriteWithResponse];
        }
    }
}

- (void)peripheral:(CBPeripheral *)periph didUpdateValueForCharacteristic:(CBCharacteristic *)charact error:(NSError *)error
{
    //labelValue.text = [[NSString alloc] initWithFormat:@"Value: %@", [charact.value description]];
    
    cValueLabel.text = [charact.value description];
}

- (void)peripheral:(CBPeripheral *)periph didWriteValueForCharacteristic:(CBCharacteristic *)charact error:(NSError *)error
{
    [periph readValueForCharacteristic: charact];
}


@end
