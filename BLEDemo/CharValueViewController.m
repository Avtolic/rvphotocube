//
//  CharValueViewController.m
//  BLEDemo
//
//  Created by Tomas Henriksson on 12/29/11.
//  Copyright (c) 2011 connectBlue. All rights reserved.
//

#import "CharValueViewController.h"
#import <CoreBluetooth/CBPeripheral.h>
#import <CoreBluetooth/CBCharacteristic.h>

@implementation CharValueViewController

@synthesize peripheral;
@synthesize service;
@synthesize characteristic;
@synthesize labelValue;
@synthesize labelService;
@synthesize labelChar;
@synthesize textFieldSetValue;
@synthesize textFieldSetValueHex;
//@synthesize textFieldValue;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle

/*
// Implement loadView to create a view hierarchy programmatically, without using a nib.
- (void)loadView
{
}
*/

// Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
- (void)viewDidLoad
{
    [super viewDidLoad];
}

- (void)viewDidUnload
{
    [self setLabelValue:nil];
    [self setTextFieldSetValue:nil];
    [self setTextFieldSetValueHex:nil];
    [self setLabelService:nil];
    [self setLabelChar:nil];
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    peripheral.delegate = self;
    textFieldSetValue.delegate = self;
    textFieldSetValueHex.delegate = self;
    
    labelService.text = [[NSString alloc] initWithFormat:@"Service: %@", service.UUID];
    labelChar.text = [[NSString alloc] initWithFormat:@"Char: %@", characteristic.UUID];
    
    //labelValue.text = characteristic.value.description;
    labelValue.text = [[NSString alloc] initWithFormat:@"Value: %@", characteristic.value];
    
    //[peripheral readValueForCharacteristic: characteristic];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

- (IBAction)setValueText:(id)sender
{
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    char buf[] = {1,2};
    
    [textField resignFirstResponder];
    
    if(textField == textFieldSetValue)
    {
        NSData *data = [[NSData alloc] initWithBytes:buf length:2];
    
        [peripheral writeValue:data forCharacteristic:characteristic type:CBCharacteristicWriteWithResponse];
    }
    else if(textField == textFieldSetValueHex)
    {

    }

    return YES;
}

- (void)peripheral:(CBPeripheral *)periph didUpdateValueForCharacteristic:(CBCharacteristic *)charact error:(NSError *)error
{
    labelValue.text = [[NSString alloc] initWithFormat:@"Value: %@", [charact.value description]];

    //labelValue.text = [charact.value description];
}

- (void)peripheral:(CBPeripheral *)peripheral didWriteValueForCharacteristic:(CBCharacteristic *)characteristic error:(NSError *)error
{
    
}

@end
