//
//  CharValueViewController.h
//  BLEDemo
//
//  Created by Tomas Henriksson on 12/29/11.
//  Copyright (c) 2011 connectBlue. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreBluetooth/CBPeripheral.h>
#import <CoreBluetooth/CBCharacteristic.h>
#import <CoreBluetooth/CBService.h>
#import <UIKit/UITextField.h>

@interface CharValueViewController : UIViewController  <UITextFieldDelegate, CBPeripheralDelegate>
{
    
}

@property (nonatomic, weak) CBPeripheral*     peripheral;
@property (nonatomic, weak) CBService*        service;
@property (nonatomic, weak) CBCharacteristic* characteristic;

@property (strong, nonatomic) IBOutlet UILabel *labelValue;
@property (strong, nonatomic) IBOutlet UILabel *labelService;
@property (strong, nonatomic) IBOutlet UILabel *labelChar;

@property (strong, nonatomic) IBOutlet UITextField *textFieldSetValue;
@property (strong, nonatomic) IBOutlet UITextField *textFieldSetValueHex;

- (IBAction)setValueText:(id)sender;

- (void)peripheral:(CBPeripheral *)peripheral didUpdateValueForCharacteristic:(CBCharacteristic *)characteristic error:(NSError *)error;

- (void)peripheral:(CBPeripheral *)peripheral didWriteValueForCharacteristic:(CBCharacteristic *)characteristic error:(NSError *)error;

- (BOOL)textFieldShouldReturn:(UITextField *)textField;

@end
