//
//  EditValueTableViewController.h
//  BLEDemo


#import <UIKit/UIKit.h>
#import <CoreBluetooth/CoreBluetooth.h>
#import <UIKit/UITextField.h>

@interface EditValueTableViewController : UITableViewController <UITextFieldDelegate, CBPeripheralDelegate>

@property (nonatomic, weak) CBPeripheral*     peripheral;
@property (nonatomic, weak) CBService*        service;
@property (nonatomic, weak) CBCharacteristic* characteristic;
@property (nonatomic, weak) NSString*         strServiceUuid;
@property (nonatomic, weak) NSString*         strCharactUuid;

@property (strong, nonatomic) IBOutlet UITextField *textValueTextField;
@property (strong, nonatomic) IBOutlet UITextField *hexValueTextField;
@property (strong, nonatomic) IBOutlet UITextField *intValueTextField;
@property (strong, nonatomic) IBOutlet UILabel *suuidLabel;
@property (strong, nonatomic) IBOutlet UILabel *cuuidLabel;
@property (strong, nonatomic) IBOutlet UILabel *cValueLabel;

- (IBAction)setValue:(id)sender;

- (void)peripheral:(CBPeripheral *)peripheral didUpdateValueForCharacteristic:(CBCharacteristic *)characteristic error:(NSError *)error;

- (void)peripheral:(CBPeripheral *)peripheral didWriteValueForCharacteristic:(CBCharacteristic *)characteristic error:(NSError *)error;

- (BOOL)textFieldShouldReturn:(UITextField *)textField;

- (void) selectTextField: (UITextField*) textField;

@end
