#import <QuartzCore/QuartzCore.h>
#import "DemoViewController.h"
#import "DiscoveredPeripheral.h"
#import <CoreBluetooth/CoreBluetooth.h>
#import <Foundation/NSException.h>
#import <AudioToolbox/AudioToolbox.h>
#import "Constants.h"
#import "BLEDefinitions.h"
// #import "ImageCache.h"

#define s1p1 @"1watch.png"
#define s1a1 @"%@/baseball.mp3"
#define s1p2 @"2watch.png"
#define s1a2 @"%@/tennis.mp3"
#define s1p3 @"3watch.png"
#define s1a3 @"%@/hockey.mp3"
#define s1p4 @"4watch.png"
#define s1a4 @"%@/football.mp3"
#define s1p5 @"5watch.png"
#define s1a5 @"%@/soccer.mp3"
#define s1p6 @"6watch.png"
#define s1a6 @"%@/basketball.mp3"

#define s2p1 @"circlewatch.png"
#define s2a1 @"%@/circlewatch2.mp3"
#define s2p2 @"pluswatch.png"
#define s2a2 @"%@/pluswatch2.mp3"
#define s2p3 @"threewavywatch.png"
#define s2a3 @"%@/threewavylineswatch2.mp3"
#define s2p4 @"squarewatch.png"
#define s2a4 @"%@/squarewatch2.mp3"
#define s2p5 @"starwatch.png"
#define s2a5 @"%@/starwatch2.mp3"
#define s2p6 @"infinitywatch.png"
#define s2a6 @"%@/infinitywatch2.mp3"

#define s3p1 @"gizapyramidwatch.png"
#define s3a1 @"%@/gizapyramidwatch2.mp3"
#define s3p2 @"stonehengewatch.png"
#define s3a2 @"%@/stonehengewatch2.mp3"
#define s3p3 @"leaningtowerofpisawatch.png"
#define s3a3 @"%@/leaningtowerofpisawatch2.mp3"
#define s3p4 @"romancolosseumwatch.png"
#define s3a4 @"%@/romancoleseumwatch2.mp3"
#define s3p5 @"greatwallofchinawatch.png"
#define s3a5 @"%@/greatwallofchinawatch2.mp3"
#define s3p6 @"rtajmahalwatch.png"
#define s3a6 @"%@/tajmahalwatch2.mp3"

#define s4p1 @"pigwatch.jpg"
#define s4a1 @"%@/pigwatch2.mp3"
#define s4p2 @"pandawatch.jpg"
#define s4a2 @"%@/pandawatch2.mp3"
#define s4p3 @"lambwatch.jpg"
#define s4a3 @"%@/lambwatch2.mp3"
#define s4p4 @"monkeywatch.jpg"
#define s4a4 @"%@/monkeywatch2.mp3"
#define s4p5 @"kittenwatch.jpg"
#define s4a5 @"%@/kittenwatch2.mp3"
#define s4p6 @"dogwatch.jpg"
#define s4a6 @"%@/puppywatch2.mp3"

#define kPhoto1 @"Photo1"
#define kPhoto2 @"Photo2"
#define kPhoto3 @"Photo3"
#define kPhoto4 @"Photo4"
#define kPhoto5 @"Photo5"
#define kPhoto6 @"Photo6"

#define kSound1 @"Sound1.caf"
#define kSound2 @"Sound2.caf"
#define kSound3 @"Sound3.caf"
#define kSound4 @"Sound4.caf"
#define kSound5 @"Sound5.caf"
#define kSound6 @"Sound6.caf"

#define knot1 @"knot1"
#define knot2 @"knot2"
#define knot3 @"knot3"
#define knot4 @"knot4"
#define knot5 @"knot5"
#define knot6 @"knot6"

static NSArray *dirPaths;
static NSString *docsDir;
NSMutableDictionary *gDictionary;
NSInteger soundoverride;
NSString *idefault;
NSString *soundbusy;
NSString *previmagesettouse;
NSInteger numberofvibes;
NSInteger loopcount;
NSInteger loopcycle;

//int _vibrateCount;
//NSTimer * vibrateTimer;

NSInteger displayValue;

int Imgvalues;
int SetImageValue;


typedef enum
{
    DEMO_S_NOT_LOADED,
    DEMO_S_DISAPPEARED,
    
    DEMO_S_APPEARED_WAIT_SERVICE_SEARCH,
    DEMO_S_APPEARED_WAIT_CHARACT_SEARCH,
    DEMO_S_APPEARED_IDLE,
    
    DEMO_S_APPEARED_NO_CONNECT_PERIPH
    
} DEMO_State;

@implementation DemoViewController
{
    DEMO_State          state;
    
    NSTimer             *timer;
    
    BOOL                redLedWriting;
    BOOL                greenLedWriting;
    
    NSMutableArray      *discoveredPeripherals;
    NSMutableArray      *connectedPeripherals;
    CBPeripheral        *connectedPeripheral;
    
    CBService           *accService;
    CBService           *tempService;
    CBService           *batteryService;
    CBService           *ledService;
    
    CBCharacteristic    *accRangeCharact;
    CBCharacteristic    *accXCharact;
    CBCharacteristic    *accYCharact;
    CBCharacteristic    *accZCharact;
    CBCharacteristic    *tempCharact;
    CBCharacteristic    *batteryCharact;
    CBCharacteristic    *greenLedCharact;
    CBCharacteristic    *redLedCharact;
    
    NSInteger   range;
    NSInteger   xval, yval, zval;
    
    UIButton *mBtnTakePhoto;
    UIButton *mBtnPhotoAlbum;
    
    BOOL traceOnOff;
    
}
@synthesize onebutton;
@synthesize peripheralSegmentedControl;
@synthesize imageset;
@synthesize activityIndicator;
@synthesize accRangeLabel;
@synthesize accXProgressView;
@synthesize accYProgressView;
@synthesize accZProgressView;
@synthesize tempLabel;
@synthesize batteryLabel;
@synthesize rssiLabel;
@synthesize greenLedSwitch;
@synthesize redLedSwitch;
@synthesize accX;
@synthesize accY;
@synthesize accZ;
@synthesize dice;
@synthesize imagesetlabel;
@synthesize iset;
@synthesize savebutton;
@synthesize sliderImgSelect;
@synthesize imageside;
@synthesize custombutton;
@synthesize custombutton2;
@synthesize custombutton3;
@synthesize helpbutton;
@synthesize groupField;
@synthesize vibrationField;
@synthesize notificationtext;



NSString *one;
NSString *two;
NSString *three;
NSString *four;
NSString *five;
NSString *six;




NSString *soundFilePath;
NSString *audioplayflag;
NSString *imagesettouse;
NSString *imagesetdefaultfield;
NSString *customized;
NSString *testsound;
NSString *soundonoff;
NSString *vibrationonoff;
NSString *pushnotificationoff;
NSString *picturedisplayonoff;


int ixx;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void) initWithPeripherals: (NSMutableArray*) dp
{
    discoveredPeripherals = dp;
    
    connectedPeripherals = [[NSMutableArray alloc] init];
    
    connectedPeripheral = nil;
    
    accService = nil;
    tempService = nil;
    batteryService = nil;
    ledService = nil;
    
    accXCharact = nil;
    accYCharact = nil;
    accZCharact = nil;
    tempCharact = nil;
    batteryCharact = nil;
    redLedCharact = nil;
    greenLedCharact = nil;
    
    redLedWriting = FALSE;
    greenLedWriting = FALSE;
    audioplayflag =  @"0";
    imagesettouse = @"1";
    testsound = @"0";
    soundonoff = @"0";
    soundbusy = @"0";
    pushnotificationoff = @"0";
    
    numberofgroups = 4;
    SetImageValue=0;
    
    
    
    
    state = DEMO_S_NOT_LOADED;
}


- (void)didReceiveMemoryWarning
{
    
    [super didReceiveMemoryWarning];
    
}

#pragma mark - View lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    //hide titlebar
    // [[UIApplication sharedApplication] setStatusBarHidden:YES withAnimation:UIStatusBarAnimationNone];
    
    _vibrateCount = 0;
    
    
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSString *idefault= [defaults objectForKey:@"imagesetdefault"];
    
    BOOL soundenabled = [defaults boolForKey:@"soundenabled"];
    BOOL vibrationenabled = [defaults boolForKey:@"vibrationenabled"];
    BOOL pushNotificationenabled = [defaults boolForKey:@"pushnotification"];
    BOOL picturedisplay = [defaults boolForKey:@"picturedisplay"];
    
    
    
    one =[defaults objectForKey:@"knot1"];
    two =[defaults objectForKey:@"knot2"];
    three =[defaults objectForKey:@"knot3"];
    four =[defaults objectForKey:@"knot4"];
    five =[defaults objectForKey:@"knot5"];
    six =[defaults objectForKey:@"knot6"];
    

   
    
    vibrationonoff = @"0";
    
    vibrationField.text = @"2";
    groupField.text = @"2";
    
    if (vibrationenabled == YES)
    {
        vibrationonoff = @"1";
    }
    
    soundoverride = 0;
    soundonoff = @"0";
    loopcycle  = 2;
    loopcount = 0;
    
    pushnotificationoff = @"0";
    
    ixx = 0;
    
    if (soundenabled == YES)
    {
        
        soundonoff = @"1";
        soundoverride = 1;
        
        custombutton2.hidden = TRUE;
    }
    
    if (pushNotificationenabled == YES)
    {
        pushnotificationoff = @"1";
        custombutton3.hidden = FALSE;
        
    }
    
    displayValue = 0;
    picturedisplayonoff = @"0";
    
    if (picturedisplay == YES)
    {
        picturedisplayonoff = @"1";
        displayValue = 1;
    }
    
    
    iset.text = idefault;
    customized = @"0";
    
    self.sliderImgSelect.hidden = YES;
    custombutton.hidden = YES;
    custombutton2.hidden = YES;
    
    
    if( [iset.text length] == 0 )
    {
        iset.text = @"1";
        NSString *idefault = iset.text;
        NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
        [defaults setObject:idefault forKey:@"imagesetdefault"];
        [defaults synchronize];
        
    }
    
    imagesettouse = iset.text;
    if ([iset.text isEqualToString:@"1"]){
        
        imageset.selectedSegmentIndex = 0;
        custombutton.hidden = YES;
        custombutton2.hidden = YES;
        self.sliderImgSelect.hidden = YES;
    }
    
    if ([iset.text isEqualToString:@"2"]){
        
        imageset.selectedSegmentIndex = 1;
        custombutton.hidden = YES;
        custombutton2.hidden = YES;
        self.sliderImgSelect.hidden = YES;
    }
    
    
    if ([iset.text isEqualToString:@"3"]){
        
        imageset.selectedSegmentIndex = 2;
        custombutton.hidden = YES;
        custombutton2.hidden = YES;
        self.sliderImgSelect.hidden = YES;
    }
    
    
    if ([iset.text isEqualToString:@"4"]){
        
        imageset.selectedSegmentIndex = 3;
        custombutton.hidden = YES;
        custombutton2.hidden = YES;
        self.sliderImgSelect.hidden = YES;
    }
    
    if ([iset.text isEqualToString:@"5"]){
        
        imageset.selectedSegmentIndex = 4;
        self.sliderImgSelect.hidden = YES;
        custombutton2.hidden = YES;
        
        if (soundenabled == YES) {
            
            custombutton2.hidden = NO;
        }
        custombutton.hidden = NO;
    }
    
    
    state = DEMO_S_DISAPPEARED;
    self.accX.text = @"X";
    self.accY.text = @"Y";
    self.accZ.text = @"Z";
    imageside.text = @"1";
    
    
//    if (displayValue == 1) {
//        
//        ImageCache * imageCache = [ImageCache sharedImageCache];
//        imageCache.image = [UIImage imageNamed:@"bagpipes.JPG"];
//        imageCache.imageID = @"0";
//        if(imageCache.image){
//            NSLog(@"image0 set");
//        }
//        
//    }
//    
//    if (displayValue == 0) {
//        
//        picturedisplayonoff = @"0";
//        
//    }
//    
    
}


- (void)viewDidUnload
{
    [self setAccXProgressView:nil];
    [self setAccYProgressView:nil];
    [self setAccZProgressView:nil];
    [self setTempLabel:nil];
    [self setGreenLedSwitch:nil];
    [self setRedLedSwitch:nil];
    [self setAccRangeLabel:nil];
    [self setBatteryLabel:nil];
    [self setActivityIndicator:nil];
    [self setPeripheralSegmentedControl:nil];
    [self setRssiLabel:nil];
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
    
    connectedPeripheral = nil;
    
    connectedPeripherals = nil;
    
    state = DEMO_S_NOT_LOADED;
    
    
    
}

- (BOOL)displayPeripheral: (CBPeripheral*)periph andCharacteristic: (CBCharacteristic*) charact
{
    char *p = (char*)charact.value.bytes;
    NSInteger val;
    BOOL ok = FALSE;
    
    if((periph == connectedPeripheral) && (p != nil))
    {
        if(charact == accRangeCharact)
        {
            range = (p[1] << 8) | p[0];
            
            accRangeLabel.text = [[NSString alloc] initWithFormat:@"Accelerometer Range +-%ldG", (long)range];
            
            ok = TRUE;
        }
        else if(charact == accXCharact)
        {
            val = p[0] + 128;
            
            accXProgressView.progress = ((float)val) / 256.0;
            accX.text = [[NSString alloc] initWithFormat:@"%ld",(long)val];
            xval = val;
            
#include "xyzroutine.h"
            
            ok = TRUE;
        }
        else if(charact == accYCharact)
        {
            val = p[0] + 128;
            
            accYProgressView.progress = ((float)val) / 256.0;
            accY.text = [[NSString alloc] initWithFormat:@"%ld",(long)val];
            yval = val;
            
#include "xyzroutine.h"
            
            ok = TRUE;
        }
        else if(charact == accZCharact)
        {
            val = p[0] + 128;
            
            accZProgressView.progress = ((float)val) / 256.0;
            accZ.text = [[NSString alloc] initWithFormat:@"%ld",(long)val];
            zval = val;
            
#include "xyzroutine.h"
            
            
            ok = TRUE;
        }
        else if(charact == tempCharact)
        {
            tempLabel.text = [[NSString alloc] initWithFormat:@"Temperature %3.2f °F", ((1.8*p[0])+32)];
            
            ok = TRUE;
        }
        else if(charact == batteryCharact)
        {
            batteryLabel.text = [[NSString alloc] initWithFormat:@"Bat %d%%", p[0]];
            
            ok = TRUE;
        }
        else if((greenLedWriting == FALSE) && (charact == greenLedCharact))
        {
            greenLedSwitch.enabled = TRUE;
            
            if(p[0] != 0)
            {
                greenLedSwitch.on = TRUE;
            }
            else
            {
                greenLedSwitch.on = FALSE;
            }
            
            [activityIndicator stopAnimating];
            
            ok = TRUE;
        }
        else if((redLedWriting == FALSE) && (charact == redLedCharact))
        {
            redLedSwitch.enabled = TRUE;
            
            if(p[0] != 0)
            {
                redLedSwitch.on = TRUE;
                audioplayflag = @"1";
            }
            else
            {
                redLedSwitch.on = FALSE;
                audioplayflag = @"0";
            }
            
            
            
            ok = TRUE;
        }
    }
    
    return ok;
}

- (BOOL) initCharacteristicForService: (CBService*)serv andCharact: (CBCharacteristic*)charact
{
    BOOL done = FALSE;
    BOOL updated;
    
    if((connectedPeripheral != nil) && (connectedPeripheral.state == CBPeripheralStateConnected))
    {
        if((serv.UUID.data.length == SERVICE_UUID_DEFAULT_LEN) &&
           (memcmp(serv.UUID.data.bytes, accServiceUuid, SERVICE_UUID_DEFAULT_LEN) == 0))
        {
            accService = serv;
            
            if((charact != nil) &&
               (charact.UUID.data.length == CHARACT_UUID_DEFAULT_LEN) &&
               (memcmp(charact.UUID.data.bytes, accRangeCharactUuid, CHARACT_UUID_DEFAULT_LEN) == 0))
            {
                accRangeCharact = charact;
                
                updated = [self displayPeripheral: connectedPeripheral andCharacteristic: accRangeCharact];
                
                if(updated == FALSE)
                    [connectedPeripheral readValueForCharacteristic: charact];
                
                done = TRUE;
            }
            else if((charact != nil) &&
                    (charact.UUID.data.length == CHARACT_UUID_DEFAULT_LEN) &&
                    (memcmp(charact.UUID.data.bytes, accXCharactUuid, CHARACT_UUID_DEFAULT_LEN) == 0))
            {
                accXCharact = charact;
                
                updated = [self displayPeripheral: connectedPeripheral andCharacteristic: accXCharact];
                
                if(updated == FALSE)
                {
                    [connectedPeripheral readValueForCharacteristic: charact];
                }
                
                [connectedPeripheral setNotifyValue: TRUE forCharacteristic:charact];
                
                done = TRUE;
            }
            else if((charact != nil) &&
                    (charact.UUID.data.length == CHARACT_UUID_DEFAULT_LEN) &&
                    (memcmp(charact.UUID.data.bytes, accYCharactUuid, CHARACT_UUID_DEFAULT_LEN) == 0))
            {
                accYCharact = charact;
                
                updated = [self displayPeripheral: connectedPeripheral andCharacteristic: accYCharact];
                
                if(updated == FALSE)
                {
                    [connectedPeripheral readValueForCharacteristic: charact];
                }
                
                [connectedPeripheral setNotifyValue: TRUE forCharacteristic:charact];
                
                done = TRUE;
            }
            else if((charact != nil) &&
                    (charact.UUID.data.length == CHARACT_UUID_DEFAULT_LEN) &&
                    (memcmp(charact.UUID.data.bytes, accZCharactUuid, CHARACT_UUID_DEFAULT_LEN) == 0))
            {
                accZCharact = charact;
                
                updated = [self displayPeripheral: connectedPeripheral andCharacteristic: accZCharact];
                
                if(updated == FALSE)
                {
                    [connectedPeripheral readValueForCharacteristic: charact];
                }
                
                [connectedPeripheral setNotifyValue: TRUE forCharacteristic:charact];
                
                done = TRUE;
            }
        }
        else if((serv.UUID.data.length == SERVICE_UUID_DEFAULT_LEN) &&
                (memcmp(serv.UUID.data.bytes, tempServiceUuid, SERVICE_UUID_DEFAULT_LEN) == 0))
        {
            tempService = serv;
            
            if((charact != nil) &&
               (charact.UUID.data.length == CHARACT_UUID_DEFAULT_LEN) &&
               (memcmp(charact.UUID.data.bytes, tempValueCharactUuid, CHARACT_UUID_DEFAULT_LEN) == 0))
            {
                tempCharact = charact;
                
                updated = [self displayPeripheral: connectedPeripheral andCharacteristic: tempCharact];
                
                if(updated == FALSE)
                    [connectedPeripheral readValueForCharacteristic: charact];
                
                [connectedPeripheral setNotifyValue: TRUE forCharacteristic:charact];
                
                done = TRUE;
            }
        }
        else if((serv.UUID.data.length == SERVICE_UUID_DEFAULT_LEN) &&
                (memcmp(serv.UUID.data.bytes, batteryServiceUuid, SERVICE_UUID_DEFAULT_LEN) == 0))
        {
            batteryService = serv;
            
            if((charact != nil) &&
               (charact.UUID.data.length == CHARACT_UUID_DEFAULT_LEN) &&
               (memcmp(charact.UUID.data.bytes, batteryLevelCharactUuid, CHARACT_UUID_DEFAULT_LEN) == 0))
            {
                batteryCharact = charact;
                
                //[periph setNotifyValue: TRUE forCharacteristic:charact];
                
                updated = [self displayPeripheral: connectedPeripheral andCharacteristic: batteryCharact];
                
                if(updated == FALSE)
                    [connectedPeripheral readValueForCharacteristic: charact];
                
                done = TRUE;
            }
        }
        else if((serv.UUID.data.length == SERVICE_UUID_DEFAULT_LEN) &&
                (memcmp(serv.UUID.data.bytes, ledServiceUuid, SERVICE_UUID_DEFAULT_LEN) == 0))
        {
            ledService = serv;
            
            if((charact != nil) &&
               (charact.UUID.data.length == CHARACT_UUID_DEFAULT_LEN) &&
               (memcmp(charact.UUID.data.bytes, greenLedCharactUuid, CHARACT_UUID_DEFAULT_LEN) == 0))
            {
                greenLedCharact = charact;
                
                updated = [self displayPeripheral: connectedPeripheral andCharacteristic: greenLedCharact];
                
                if(updated == FALSE)
                    [connectedPeripheral readValueForCharacteristic: charact];
                
                done = TRUE;
            }
            else if((charact != nil) &&
                    (charact.UUID.data.length == CHARACT_UUID_DEFAULT_LEN) &&
                    (memcmp(charact.UUID.data.bytes, redLedCharactUuid, CHARACT_UUID_DEFAULT_LEN) == 0))
            {
                redLedCharact = charact;
                
                updated = [self displayPeripheral: connectedPeripheral andCharacteristic: redLedCharact];
                
                if(updated == FALSE)
                    [connectedPeripheral readValueForCharacteristic: charact];
                
                done = TRUE;
            }
        }
    }
    
    return done;
}


- (void) initConnectedPeripheral
{
    CBService*          service;
    CBCharacteristic*   charact;
    BOOL                ok;
    
    accRangeLabel.text = @"---";
    accXProgressView.progress = 0;
    accYProgressView.progress = 0;
    accZProgressView.progress = 0;
    tempLabel.text = @"---";
    rssiLabel.text = @"---";
    batteryLabel.text = @"";
    redLedSwitch.enabled = FALSE;
    greenLedSwitch.enabled = FALSE;
    
    accService = nil;
    tempService = nil;
    batteryService = nil;
    ledService = nil;
    
    accRangeCharact = nil;
    accXCharact = nil;
    accYCharact = nil;
    accZCharact = nil;
    tempCharact = nil;
    batteryCharact = nil;
    redLedCharact = nil;
    greenLedCharact = nil;
    
    connectedPeripheral.delegate = self;
    
    if(connectedPeripheral.services != nil)
    {
        for(int i = 0; i < connectedPeripheral.services.count; i++)
        {
            service = [connectedPeripheral.services objectAtIndex:i];
            
            if((service.characteristics != nil) && (service.characteristics.count > 0))
            {
                for(int j = 0; j < service.characteristics.count; j++)
                {
                    charact = [service.characteristics objectAtIndex:j];
                    
                    ok = [self initCharacteristicForService:service andCharact:charact];
                }
            }
            else
            {
                ok = [self initCharacteristicForService:service andCharact:nil];
            }
        }
    }
    
    NSMutableArray *arr = [[NSMutableArray alloc] init];
    NSData *data;
    CBUUID *uuid;
    
    if(ledService == nil)
    {
        data = [NSData dataWithBytes: ledServiceUuid length: SERVICE_UUID_DEFAULT_LEN];
        uuid = [CBUUID UUIDWithData: data];
        [arr addObject:uuid];
    }
    else if((redLedCharact == nil) || (greenLedCharact == nil))
    {
        [connectedPeripheral discoverCharacteristics:nil forService: ledService];
    }
    
    if(tempService == nil)
    {
        data = [NSData dataWithBytes: tempServiceUuid length: SERVICE_UUID_DEFAULT_LEN];
        uuid = [CBUUID UUIDWithData: data];
        [arr addObject:uuid];
    }
    else if(tempCharact == nil)
    {
        [connectedPeripheral discoverCharacteristics:nil forService: tempService];
    }
    
    if(batteryService == nil)
    {
        data = [NSData dataWithBytes: batteryServiceUuid length: SERVICE_UUID_DEFAULT_LEN];
        uuid = [CBUUID UUIDWithData: data];
        [arr addObject:uuid];
    }
    else if(batteryCharact == nil)
    {
        [connectedPeripheral discoverCharacteristics:nil forService: batteryService];
    }
    
    if(accService == nil)
    {
        data = [NSData dataWithBytes: accServiceUuid length: SERVICE_UUID_DEFAULT_LEN];
        uuid = [CBUUID UUIDWithData: data];
        [arr addObject:uuid];
    }
    else if((accRangeCharact == nil) || (accXCharact == nil) || (accYCharact == nil) || (accZCharact == nil))
    {
        [connectedPeripheral discoverCharacteristics:nil forService: accService];
    }
    
    if(arr.count > 0)
    {
        state = DEMO_S_APPEARED_WAIT_SERVICE_SEARCH;
        
        [connectedPeripheral discoverServices:arr];
    }
    else
    {
        state = DEMO_S_APPEARED_IDLE;
    }
}

- (void)viewDidAppear:(BOOL)animated
{
    DiscoveredPeripheral* dp;
    
    [super viewDidAppear:animated];
    
    connectedPeripheral = nil;
    [connectedPeripherals removeAllObjects];
    
    for(int i = 0; i < discoveredPeripherals.count; i++)
    {
        dp = [discoveredPeripherals objectAtIndex:i];
        
        if (dp.peripheral.state == CBPeripheralStateConnected)
        {
            //if(connectedPeripheral == nil)
            //    connectedPeripheral = dp.peripheral;
            
            [connectedPeripherals addObject:dp.peripheral];
        }
    }
    
    [peripheralSegmentedControl removeAllSegments];
    
    if(connectedPeripherals.count > 0)
    {
        for(int i = 0; i < connectedPeripherals.count; i++)
        {
            [peripheralSegmentedControl insertSegmentWithTitle:[[connectedPeripherals objectAtIndex:i] name] atIndex:i animated:TRUE];
        }
        
        if((peripheralSegmentedControl.selected == FALSE) ||
           (peripheralSegmentedControl.selectedSegmentIndex >= connectedPeripherals.count))
        {
            peripheralSegmentedControl.selectedSegmentIndex = 0;
            connectedPeripheral = [connectedPeripherals objectAtIndex:0];
        }
        else
        {
            connectedPeripheral = [connectedPeripherals objectAtIndex:peripheralSegmentedControl.selectedSegmentIndex];
        }
        
        if(connectedPeripheral != nil)
        {
            //[connectedPeripheral readRSSI];
            
            self->timer = [NSTimer scheduledTimerWithTimeInterval:0.5 target: self selector:@selector(timeout:) userInfo:nil repeats:TRUE];
        }
    }
    else
    {
        [peripheralSegmentedControl insertSegmentWithTitle:@"No Connected Peripheral" atIndex:0 animated:TRUE];
        
    }
    
    activityIndicator.hidesWhenStopped = TRUE;
    
    redLedWriting = FALSE;
    greenLedWriting = FALSE;
    
    if(connectedPeripheral != nil)
    {
        [activityIndicator startAnimating];
        
        [self initConnectedPeripheral];
    }
    else
    {
        [activityIndicator stopAnimating];
        
        accRangeLabel.text = @"Accelerometer Range Not Available";
        accXProgressView.progress = 0;
        accYProgressView.progress = 0;
        accZProgressView.progress = 0;
        tempLabel.text = @"Temperature Not Available";
        rssiLabel.text = @"RSSI Value Not Available";
        batteryLabel.text = @"";
        redLedSwitch.enabled = FALSE;
        greenLedSwitch.enabled = FALSE;
        
        accService = nil;
        tempService = nil;
        batteryService = nil;
        ledService = nil;
        
        accRangeCharact = nil;
        accXCharact = nil;
        accYCharact = nil;
        accZCharact = nil;
        tempCharact = nil;
        batteryCharact = nil;
        redLedCharact = nil;
        greenLedCharact = nil;
        
        
        state = DEMO_S_APPEARED_NO_CONNECT_PERIPH;
    }
    
}

- (void) viewWillDisappear:(BOOL)animated
{
    CBPeripheral* p;
    
    [super viewWillDisappear:animated];
    
    [activityIndicator stopAnimating];
    
    for(int i = 0; i < connectedPeripherals.count; i++)
    {
        p = [connectedPeripherals objectAtIndex:i];
        
        if(p.state == CBPeripheralStateConnected)
        {
            if(accXCharact != nil)
                [p setNotifyValue: FALSE forCharacteristic:accXCharact];
            
            if(accYCharact != nil)
                [p setNotifyValue: FALSE forCharacteristic:accYCharact];
            
            if(accZCharact != nil)
                [p setNotifyValue: FALSE forCharacteristic:accZCharact];
            
            if(tempCharact != nil)
                [p setNotifyValue: FALSE forCharacteristic:tempCharact];
            
            //if(batteryCharact != nil)
            //    [p setNotifyValue: FALSE forCharacteristic:batteryCharact];
        }
        
        p.delegate = nil;
    }
    
    if(self->timer != nil)
    {
        [self->timer invalidate];
        self->timer = nil;
    }
    
}

- (void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
    
    state = DEMO_S_DISAPPEARED;
}



- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

- (IBAction)selectPeripheral:(id)sender
{
    CBPeripheral *selectedPeripheral = nil;
    
    if(connectedPeripherals.count > 0)
    {
        if(peripheralSegmentedControl.selectedSegmentIndex < connectedPeripherals.count)
        {
            selectedPeripheral = [connectedPeripherals objectAtIndex:peripheralSegmentedControl.selectedSegmentIndex];
            
            if((selectedPeripheral != nil) && (selectedPeripheral != connectedPeripheral))
            {
                connectedPeripheral.delegate = nil;
                
                connectedPeripheral = selectedPeripheral;
                
                [activityIndicator startAnimating];
                
                [self initConnectedPeripheral];
            }
        }
    }
    else
    {
        peripheralSegmentedControl.selectedSegmentIndex = -1;
    }
}

- (IBAction)greenLedSwitchChanged:(id)sender
{
    NSData *data = nil;
    unsigned char buf[1] = {0};
    
    if(greenLedSwitch.isOn == TRUE)
        buf[0] = 1;
    
    data = [NSData dataWithBytes:buf length:1];
    
    greenLedWriting = TRUE;
    
    [connectedPeripheral writeValue:data forCharacteristic:greenLedCharact type:CBCharacteristicWriteWithResponse];
}

- (IBAction)redLedSwitchChanged:(id)sender
{
    NSData *data = nil;
    unsigned char buf[1] = {0};
    
    if(redLedSwitch.isOn == TRUE)
        buf[0] = 1;
    
    data = [NSData dataWithBytes:buf length:1];
    
    redLedWriting = TRUE;
    
    [connectedPeripheral writeValue:data forCharacteristic:redLedCharact type:CBCharacteristicWriteWithResponse];
    audioplayflag = @"0";
    
    if(redLedSwitch.isOn == TRUE)
        audioplayflag = @"1";
    
}

- (void)peripheral:(CBPeripheral *)periph didDiscoverServices:(NSError *)error
{
    CBService   *s;
    
    if(periph == connectedPeripheral)
    {
        for(int i = 0; i < periph.services.count; i++)
        {
            s = [[periph services] objectAtIndex:i];
            
            [periph discoverCharacteristics:nil forService: s];
            
            state = DEMO_S_APPEARED_WAIT_CHARACT_SEARCH;
        }
    }
}

- (void)peripheral:(CBPeripheral *)periph didDiscoverCharacteristicsForService:(CBService *)serv error:(NSError *)error
{
    CBCharacteristic* charact;
    BOOL ok;
    
    if(periph == connectedPeripheral)
    {
        for(int i = 0; i < serv.characteristics.count; i++)
        {
            charact = [serv.characteristics objectAtIndex:i];
            
            ok = [self initCharacteristicForService:serv andCharact:charact];
        }
    }
}

- (void)peripheral:(CBPeripheral *)periph didUpdateValueForCharacteristic:(CBCharacteristic *)charact error:(NSError *)error
{
    if(error == nil)
    {
        [self displayPeripheral: periph andCharacteristic: charact];
    }
}

- (void)peripheral:(CBPeripheral *)periph didWriteValueForCharacteristic:(CBCharacteristic *)charact error:(NSError *)error
{
    if(periph == connectedPeripheral)
    {
        if(charact == greenLedCharact)
            greenLedWriting = FALSE;
        else if(charact == redLedCharact)
            redLedWriting = FALSE;
        
        [periph readValueForCharacteristic: charact];
    }
}

- (void)peripheralDidUpdateRSSI:(CBPeripheral *)periph error:(NSError *)error
{
    if((periph == connectedPeripheral) && (error == nil))
    {
        rssiLabel.text = [[NSString alloc] initWithFormat:@"RSSI %@ dB", periph.RSSI];
    }
}

- (void) timeout: (NSTimer*)tm
{
    if(self->timer == tm)
    {
        if((connectedPeripheral != nil) && (connectedPeripheral.state == CBPeripheralStateConnected))
        {
            [connectedPeripheral readRSSI];
        }
    }
}



-(void) imagesetIndexChanged
{
    
    
    switch (self.imageset.selectedSegmentIndex)
    {
        case 0:
            imagesettouse = @"1";
            self.imagesetlabel.text = [[NSString alloc] initWithFormat:@"%@",@" NUMBERS "];
            custombutton.hidden = TRUE;
            custombutton2.hidden = TRUE;
            iset.text = @"1";
            SetImageValue = 0;
            
            break;
        case 1:
            imagesettouse = @"2";
            self.imagesetlabel.text = [[NSString alloc] initWithFormat:@"%@",@" ESP "];
            iset.text = @"2";
            custombutton.hidden = TRUE;
            custombutton2.hidden = TRUE;
            SetImageValue = 1;
            break;
        case 2:
            imagesettouse = @"3";
            self.imagesetlabel.text = [[NSString alloc] initWithFormat:@"%@",@" 6-WONDERS "];
            iset.text = @"3";
            custombutton.hidden = TRUE;
            custombutton2.hidden = TRUE;
            SetImageValue = 2;
            break;
        case 3:
            imagesettouse = @"4";
            self.imagesetlabel.text = [[NSString alloc] initWithFormat:@"%@",@" ANIMALS "];
            iset.text = @"4";
            custombutton.hidden = TRUE;
            custombutton2.hidden = TRUE;
            SetImageValue = 3;
            break;
            
        case 4:
            imagesettouse = @"5";
            self.imagesetlabel.text = [[NSString alloc] initWithFormat:@"%@",@" CUSTOM "];
            iset.text = @"5";
            custombutton.hidden = FALSE;
            SetImageValue = 4;
            
            
            if (soundoverride == 1) {
                custombutton2.hidden = FALSE;
                
            }
            
            if (soundoverride == 0) {
                soundonoff = @"0";
                
                custombutton2.hidden = TRUE;
            }
            
            break;
            
        default:
            break;
            
    }
    NSString *idefault = iset.text;
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults setObject:idefault forKey:@"imagesetdefault"];
    [defaults synchronize];
    
#include "xyzroutine.h"
    
    
    if ([imagesettouse isEqualToString:@"5"]) {
        self.sliderImgSelect.hidden = NO;
    } else {
        self.sliderImgSelect.hidden = YES;
    }
    
}


-(IBAction)custombuttonclick:(id)sender
{
    NSString *lastimage = imageside.text;
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults setObject:lastimage forKey:@"lastimage"];
    [defaults synchronize];
}
-(IBAction)helpbuttonclick:(id)sender
{
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"http://www.skindellmagic.com/rvphotohelp2012.html"]];
}

+ (NSString*)retrieveFromUserDefaults:(NSString*)key
{
    NSUserDefaults *standardUserDefaults = [NSUserDefaults standardUserDefaults];
    NSString *val = nil;
    
    if (standardUserDefaults)
        val = [standardUserDefaults objectForKey:key];
    
    // TODO: / apparent Apple bug: if user hasn't opened Settings for this app yet (as if?!), then
    // the defaults haven't been copied in yet.  So do so here.  Adds another null check
    // for every retrieve, but should only trip the first time
    if (val == nil) {
        NSLog(@"user defaults may not have been loaded from Settings.bundle ... doing that now ...");
        //Get the bundle path
        NSString *bPath = [[NSBundle mainBundle] bundlePath];
        NSString *settingsPath = [bPath stringByAppendingPathComponent:@"Settings.bundle"];
        NSString *plistFile = [settingsPath stringByAppendingPathComponent:@"Root.plist"];
        
        //Get the Preferences Array from the dictionary
        NSDictionary *settingsDictionary = [NSDictionary dictionaryWithContentsOfFile:plistFile];
        NSArray *preferencesArray = [settingsDictionary objectForKey:@"PreferenceSpecifiers"];
        
        //Loop through the array
        NSDictionary *item;
        for(item in preferencesArray)
        {
            //Get the key of the item.
            NSString *keyValue = [item objectForKey:@"Key"];
            
            //Get the default value specified in the plist file.
            id defaultValue = [item objectForKey:@"DefaultValue"];
            
            if (keyValue && defaultValue) {
                [standardUserDefaults setObject:defaultValue forKey:keyValue];
                if ([keyValue compare:key] == NSOrderedSame)
                    val = defaultValue;
            }
        }
        [standardUserDefaults synchronize];
    }
    return val;
}

+ (void)saveToUserDefaults:(NSString*)key value:(NSString*)valueString
{
    NSUserDefaults *standardUserDefaults = [NSUserDefaults standardUserDefaults];
    
    if (standardUserDefaults) {
        [standardUserDefaults setObject:valueString forKey:key];
        [standardUserDefaults synchronize];
    } else {
        
    }
}

-(IBAction)onebuttonclick:(id)sender {
}

-(IBAction)icaptureclick:(id)sender
{
    
    UIActionSheet *sheet = [[UIActionSheet alloc] initWithTitle:@"Please select." delegate:self cancelButtonTitle:nil destructiveButtonTitle:nil otherButtonTitles:nil];
    [sheet addButtonWithTitle:NSLocalizedString(@"Take a Photo", nil)];
    [sheet addButtonWithTitle:NSLocalizedString(@"Use a Photo Album", nil)];
    sheet.cancelButtonIndex = [sheet addButtonWithTitle:NSLocalizedString(@"Cancel", nil)];
    [sheet showInView:self.view];
    
    
}

- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
    NSString *title = [actionSheet buttonTitleAtIndex:buttonIndex];
    if ([title isEqual:NSLocalizedString(@"Take a Photo", nil)]) {
        if ([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera] == NO)
        {
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Device" message:@"App can't open camera device" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
            [alert show];
            return;
        }
        
        UIImagePickerController *controller = [[UIImagePickerController alloc] init];
        controller.sourceType = UIImagePickerControllerSourceTypeCamera;
        controller.allowsEditing = TRUE;
        controller.delegate = self;
        [self presentViewController:controller animated:NO completion:nil];
    } else if ([title isEqual:NSLocalizedString(@"Use a Photo Album", nil)]) {
        if ([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypePhotoLibrary] == NO) {
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Device" message:@"App can't open photo library" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
            [alert show];
            return;
        }
        UIImagePickerController *controller = [[UIImagePickerController alloc] init];
        controller.allowsEditing = TRUE;
        controller.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
        controller.delegate = self;
        [self presentViewController:controller animated:NO completion:nil];
    }
}

#pragma mark UIImagePickerControllerDelegate
- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    [picker dismissViewControllerAnimated:YES completion:nil];
    UIImage *selectedImg = [info objectForKey:UIImagePickerControllerEditedImage];
    float width = selectedImg.size.width;
    float height = selectedImg.size.height;
    float newWidth = height*dice.frame.size.width/dice.frame.size.height;
    float newHeight = selectedImg.size.height;
    if (newWidth > width) {
        newWidth = width;
        newHeight = width*dice.frame.size.height/dice.frame.size.width;
    }
    CGRect rect = CGRectMake(width/2-newWidth/2, height/2-newHeight/2, newWidth, newHeight);
    selectedImg = [UIImage imageWithCGImage:CGImageCreateWithImageInRect(selectedImg.CGImage, rect)];
    
    dice.image = selectedImg;
    idefault = imageside.text;
    if([idefault isEqualToString: @"1"]){
        [[NSUserDefaults standardUserDefaults] setObject:UIImageJPEGRepresentation(selectedImg, 1.0) forKey:kPhoto1];
    }
    if([idefault isEqualToString: @"2"]){
        [[NSUserDefaults standardUserDefaults] setObject:UIImageJPEGRepresentation(selectedImg, 1.0) forKey:kPhoto2];
    }
    if([idefault isEqualToString: @"3"]){
        [[NSUserDefaults standardUserDefaults] setObject:UIImageJPEGRepresentation(selectedImg, 1.0) forKey:kPhoto3];
    }
    if([idefault isEqualToString: @"4"]){
        [[NSUserDefaults standardUserDefaults] setObject:UIImageJPEGRepresentation(selectedImg, 1.0) forKey:kPhoto4];
    }
    if([idefault isEqualToString: @"5"]){
        [[NSUserDefaults standardUserDefaults] setObject:UIImageJPEGRepresentation(selectedImg, 1.0) forKey:kPhoto5];
    }
    
    if([idefault isEqualToString: @"6"]){
        [[NSUserDefaults standardUserDefaults] setObject:UIImageJPEGRepresentation(selectedImg, 1.0) forKey:kPhoto6];
    }
}
- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker
{
    [picker dismissViewControllerAnimated:YES completion:nil];
}
- (IBAction)vibrate:(id)sender {
    /*
     numberofvibes = [self.vibrationField.text intValue];
     numberofgroups = [self.groupField.text intValue];
     */
    if(numberofgroups > 0 && numberofvibes > 0)
    {
        //    NSLog(@"Vibration Count: %d Group Count: %d", numberofvibes, numberofgroups);
        groupCount = 0;
        //    [self.vibrateButton setEnabled:NO];
        //     [self.vibrateButton setTitle:@"Vibrating..." forState:UIControlStateNormal];
        bigTimer = [NSTimer scheduledTimerWithTimeInterval:1 target:self selector:@selector(startGroup) userInfo:nil repeats:YES];
    }
}
- (void)startGroup
{
    if(groupCount < numberofgroups)
    {
        if(![vibrateTimer isValid])
        {
            groupCount++;
            //   NSLog(@"Group %d started", groupCount);
            _vibrateCount = 0;
            vibrateTimer = [NSTimer scheduledTimerWithTimeInterval:.5 target:self selector:@selector(vibratePhone) userInfo:nil repeats:YES];
        }
    } else {
        [bigTimer invalidate];
    }
}
-(void)vibratePhone
{
    _vibrateCount++;
    if(_vibrateCount <= numberofvibes) {
        //   NSLog(@"Vibrate %d", _vibrateCount);
        AudioServicesPlaySystemSound(kSystemSoundID_Vibrate);
    }
    else if(_vibrateCount >= numberofvibes + 1){ // wait 1 sec more for pause
        [vibrateTimer invalidate];
        if(groupCount >= numberofgroups)
        {
            //      NSLog(@"Vibration Finish");
            [self performSelectorOnMainThread:@selector(EnableButton) withObject:nil waitUntilDone:NO];
        }
    }
}
- (void)EnableButton
{
    [self.vibrateButton setEnabled:YES];
    [self.vibrateButton setTitle:@"Vibrate!" forState:UIControlStateNormal];
}
- (void) showMessage:(NSString *)message
{
    [self hideAlert:nil];
    altView = [[UIAlertView alloc]initWithTitle:@"Photo Cube" message:message delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles: nil];
    [altView show];
    [NSTimer scheduledTimerWithTimeInterval:AutoHideAlertInterval target:self selector:@selector(hideAlert:) userInfo:nil repeats:NO];
}
-(void)hideAlert:(id)sender
{
    [altView dismissWithClickedButtonIndex:0 animated:YES];
}
@end
