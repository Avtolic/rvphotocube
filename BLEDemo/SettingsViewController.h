
#import <UIKit/UIKit.h>
#import <AVFoundation/AVFoundation.h>

@interface SettingsViewController : UIViewController
{
    
}

@property (strong,nonatomic) IBOutlet UISwitch *vibrationswitch;
@property (strong,nonatomic) IBOutlet UISwitch *soundswitch;
@property (strong,nonatomic) IBOutlet UISwitch *notificationswitch;
@property (strong,nonatomic) IBOutlet UISwitch *watchswitch;
@property (strong,nonatomic) IBOutlet UIButton *saveallbutton;


- (IBAction)vibrationswitchclick:(id)sender;
- (IBAction)soundswitchclick:(id)sender;
- (IBAction)notificationswitchclick:(id)sender;
- (IBAction)watchswitchclick:(id)sender;
- (IBAction)saveallbuttonclick:(id)sender;



@end